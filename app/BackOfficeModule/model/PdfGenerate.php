<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 25.10.2017
 * Time: 8:15
 */

namespace App\BackOfficeModule\Model;

use Dompdf\Dompdf;

class PdfGenerate
{
    public function pdfGenerate(){
        $dompdf = new DOMPDF();
        $dompdf->load_html("aaa");
        $dompdf->set_paper('a4', 'portrait');
        $dompdf->render();
        file_put_contents('my_pdf.pdf', $dompdf->output());

        //$dompdf->stream("dompdf_out.pdf", array("Attachment" => true));
        //exit(0);
    }
}