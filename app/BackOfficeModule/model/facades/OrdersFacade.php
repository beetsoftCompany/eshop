<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 30.10.2017
 * Time: 10:40
 */

namespace App\BackOfficeModule\Model\Facades;

use Kdyby\Doctrine\EntityManager;
use App\Models\Entities\Orders as OrdersEntity;
use App\Models\Facades\DeliveryMethodsFacade;
use App\Models\Facades\PaymentMethodsFacade;
use App\BackOfficeModule\Model\Orders;
use Nette\SmartObject;

class OrdersFacade
{
	use SmartObject;

    /** @var EntityManager  */
    private $EntityManager;

    /** @var \App\Models\Facades\DeliveryMethodsFacade  */
    private $deliveryMethodsFacade;

    /** @var \App\Models\Facades\PaymentMethodsFacade  */
    private $paymentMethodsFacade;

    private $orders;

    /**
     * OrdersFacade constructor.
     * @param EntityManager $EntityManager
     */
    public function __construct(EntityManager $EntityManager, DeliveryMethodsFacade $deliveryMethodsFacade, PaymentMethodsFacade $paymentMethodsFacade, Orders $orders)
    {
        $this->EntityManager = $EntityManager;
        $this->deliveryMethodsFacade = $deliveryMethodsFacade;
        $this->paymentMethodsFacade = $paymentMethodsFacade;
        $this->orders = $orders;
    }

    /**
     * @param $id
     * @return null|object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getOrder($id){
        return isset($id) ? $this->EntityManager->find(OrdersEntity::getClassName(), $id) : NULL;
    }

    /**
     * @return object
     */
    public function getOrders(){
        $orders = $this->EntityManager->getRepository(OrdersEntity::getClassName())->findAll();
        $ordersArray = [];
        foreach($orders as $order){
            $orderArray = (array)$order;/*
            $ordersArray[] = (object)[
                'id' => $order->id,
                'firstname' => $order->firstname,
                'surname' => $order->surname,
                'delivery_method' => $this->deliveryMethodsFacade->getDeliveryMethod($order->deliveryMethod),
                'payment_method' => $this->paymentMethodsFacade->getPaymentMethod($order->paymentMethod),
                'date_of_entry' => $order->date_of_entry,
                'total_price' => $this->orders->getTotalOrderPrice($order->id)
            ];*/
            $orderArray['deliveryMethod'] = $this->deliveryMethodsFacade->getDeliveryMethod($order->deliveryMethod);
            $orderArray['paymentMethod'] = $this->paymentMethodsFacade->getPaymentMethod($order->paymentMethod);
            $orderArray['totalPrice'] = $this->orders->getTotalOrderPrice($order->id);

            $ordersArray[] = (object)$orderArray;
        }
        return $ordersArray;
    }
    public function todayOrdersCount(){
        $now = new \DateTime();
        return $this->EntityManager->createQueryBuilder()
            ->select("COUNT(o) as todayOrdersCount")
            ->from(OrdersEntity::class, "o")
            ->where('YEAR(o.date_of_entry) = :year')
            ->andWhere('MONTH(o.date_of_entry) = :month')
            ->andWhere('DAY(o.date_of_entry) = :day')
            ->setParameter("year", $now->format("Y"))
            ->setParameter("month", $now->format("m"))
            ->setParameter("day", $now->format("d"))
            ->getQuery()
            ->getSingleScalarResult();
    }
}