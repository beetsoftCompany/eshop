<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 04.10.2017
 * Time: 18:36
 */

namespace App\BackOfficeModule\Model;

use Nette;
use Kdyby\Doctrine\EntityManager;
use App\Models\Entities\OrderItems;
use App\Models\Entities\Items;
use App\BackOfficeModule\Model\Facades\AdministrationSettingsFacade;

class Orders
{
	use Nette\SmartObject;

    /** @var AdministrationSettingsFacade  */
    private $administrationSettingsFacade;

    /** @var EntityManager  */
    private $EntityManager;

    public function __construct(EntityManager $entityManager, AdministrationSettingsFacade $administrationSettingsFacade)
    {
        $this->EntityManager = $entityManager;
        $this->administrationSettingsFacade = $administrationSettingsFacade;
    }

    /**
     * @param $orderId
     * @return object
     */
    public function getTotalOrderPrice($orderId){
        $orderItems = $this->EntityManager->getRepository(OrderItems::class)->findByOrderId($orderId);
        $totalPrice = ["price" => 0, "priceWithDPH" => 0];
        foreach($orderItems as $orderItem){
            $item = $this->EntityManager->getRepository(Items::class)->find($orderItem->item);
            $itemPrice = $item->price;
            $dph = $this->administrationSettingsFacade->getSetting($item->dph);
            $itemPriceWithQuantity = $itemPrice * $orderItem->quantity;
            $itemPriceWithDph = $itemPriceWithQuantity + $itemPriceWithQuantity * $dph * 0.01;
            $totalPrice["price"] += $itemPriceWithQuantity;
            $totalPrice["priceWithDPH"] += $itemPriceWithDph;
        }
        return (object)$totalPrice;
    }
}