<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 23.02.2018
 * Time: 16:24
 */

namespace App\BackOfficeModule\Model;

use Nette;
use Kdyby\Doctrine\EntityManager;
use App\Models\Entities\Settings;

class StatisticsInfo
{
	use Nette\SmartObject;

    /** @var EntityManager  */
    private $entityManager;

    /**
     * StatisticsInfo constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getMostUsedOS(){
        return $this->entityManager->createQueryBuilder()
            ->select('s')
            ->from(Settings::class, "s")
            ->groupBy("s.itemId")
            ->orderBy("countOfVisits", "DESC")
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }
}