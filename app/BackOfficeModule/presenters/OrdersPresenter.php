<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 04.10.2017
 * Time: 18:00
 */

namespace App\BackOfficeModule\Presenters;

use Nette;
use App\Models\Entities\OrderItems;

class OrdersPresenter extends BasePresenter
{
    /** @var  \App\BackOfficeModule\Model\Orders @inject */
    public $orders;
    /** @var  \App\BackOfficeModule\Model\Facades\AdministrationSettingsFacade @inject */
    public $administrationSettingsFacade;
    /** @var  \App\BackOfficeModule\Model\PdfGenerate @inject */
    public $pdfGenerate;


    /** @var \App\BackOfficeModule\Model\Facades\OrdersFacade @inject */
    public $ordersFacade;
/*
    const

    ;*/

    public function renderDefault(){
        $orders = $this->ordersFacade->getOrders();
        $this->template->orders = $orders;
    }
    public function renderOrder($orderId){
        $this->getOrder($orderId);
    }

    public function renderInvoice($orderId){
        $this->getOrder($orderId);
    }

    function getOrder($orderId){
        $orderItems = $this->EntityManager->getRepository(OrderItems::class)->getOrderItems($orderId);
        $orderItemsArray = [];
        $totalPrice = ['withoutDph' => 0, 'withDph' => 0];
        foreach ($orderItems as $orderItem) {
            $item = $orderItem['item'];
            $itemQuantity = $orderItem['quantity'];
            $dph = $this->settingsFacade->getSetting($item->dph);
            $itemPrice = $item->price;

            $item = (array)$item;
            $item['priceWithDph'] = $itemPrice + $itemPrice * $dph * 0.01;
            $item['dph'] = $dph;
            $item['totalPrice'] = $itemPrice * $itemQuantity;
            $item['totalPriceWithDph'] = ($itemPrice + $itemPrice * $dph * 0.01) * $itemQuantity;
            $item['orderQuantity'] = $itemQuantity;
            $orderItemsArray[] = (object)$item;

            $totalPrice['withoutDph'] += $item['totalPrice'];
            $totalPrice['withDph'] += $item['totalPriceWithDph'];
        }
        $orderItems = $orderItemsArray;

        $this->template->orderItems = $orderItems;
        $this->template->order = $this->ordersFacade->getOrder($orderId);
        $this->template->totalPrice = (object)$totalPrice;
    }
}