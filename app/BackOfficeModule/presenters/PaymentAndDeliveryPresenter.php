<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 04.10.2017
 * Time: 18:12
 */

namespace App\BackOfficeModule\Presenters;

use App\Models\Entities\DeliveryMethods;
use App\Models\Entities\PaymentMethods;
use App\Models\Entities\PaymentMethodsOfDeliveryMethods;

class PaymentAndDeliveryPresenter extends BasePresenter
{
    /** @var  \App\BackOfficeModule\Factories\AddDeliveryMethodFormFactory @inject */
    public $addDeliveryMethodFormFactory;
    /** @var  \App\BackOfficeModule\Factories\AddpaymentMethodFormFactory @inject */
    public $addPaymentMethodFormFactory;
    /** @var  \App\BackOfficeModule\Factories\EditDeliveryMethodFormFactory @inject */
    public $editDeliveryMethodFormFactory;
    /** @var  \App\BackOfficeModule\Factories\EditPaymentMethodFormFactory @inject */
    public $editPaymentMethodFormFactory;

    public function startup(){
        parent::startup();
        //$this->template->editedDeliveryMethod = new DeliveryMethods();
        //$this->template->editedPaymentMethod = new PaymentMethods();
    }

    public function renderDefault()
    {
        $this->template->deliveryMethods = $this->EntityManager->getRepository(DeliveryMethods::class)->findDeliveryMethodsWithPaymentMethods();
        $this->template->paymentMethods = $this->EntityManager->getRepository(PaymentMethods::class)->findAll();
    }

    function createComponentAddDeliveryMethod()
    {
        $form = $this->addDeliveryMethodFormFactory->create();
        return $form;
    }

    function createComponentAddPaymentMethod()
    {
        $form = $this->addPaymentMethodFormFactory->create();
        return $form;
    }

    function createComponentEditDeliveryMethod()
    {
        $form = $this->editDeliveryMethodFormFactory->create();
        return $form;
    }

    function createComponentEditPaymentMethod()
    {
        $form = $this->editPaymentMethodFormFactory->create();
        return $form;
    }

    public function handleDeleteDeliveryMethod($methodId)
    {
        if ($methodId != null) {
            $deliveryMethod = $this->EntityManager->find(DeliveryMethods::class, $methodId);
            if ($deliveryMethod != null) {
                $this->EntityManager->remove($deliveryMethod);
                //$this->EntityManager->remove($this->EntityManager->getRepository(PaymentMethodsOfDeliveryMethods::class)->findBy(["deliveryMethodId" => $methodId]));
                $this->EntityManager->remove($this->EntityManager->getRepository(PaymentMethodsOfDeliveryMethods::class)->findByDeliveryMethodId($methodId));
                $this->EntityManager->flush();
            }
            if ($this->isAjax()) {
                $this->redrawControl("deliveryMethods");
            }
        }
    }

    public function handleDeletePaymentMethod($methodId)
    {
        if ($methodId != null) {
            $paymentMethod = $this->EntityManager->find(PaymentMethods::class, $methodId);
            if ($paymentMethod != null) {
                $this->EntityManager->remove($paymentMethod);
                $this->EntityManager->remove($this->EntityManager->getRepository(PaymentMethodsOfDeliveryMethods::class)->findBy(["paymentMethodId" => $methodId]));
                $this->EntityManager->flush();
            }
            if ($this->isAjax()) {
                $this->redrawControl("paymentMethods");
            }
        }
    }

    public function handleGetEditedDeliveryMethod($methodId){
        if ($this->isAjax()) {
            $this->template->editedDeliveryMethod = $this->EntityManager->find(DeliveryMethods::class, $methodId);;
            $this->redrawControl("deliveryMethodEdit");
        }
    }
    public function handleGetEditedPaymentMethod($methodId){
        if ($this->isAjax()) {
            $this->template->editedPaymentMethod = $this->EntityManager->find(PaymentMethods::class, $methodId);;
            $this->redrawControl("paymentMethodEdit");
        }
    }
}