<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 04.10.2017
 * Time: 18:09
 */

namespace App\BackOfficeModule\Presenters;

use Nette;
use Nette\Application\UI\Form;
use Nette\Utils\Strings;

class ProductsPresenter extends BasePresenter
{
    /** @var  \App\Models\ItemCategories @inject */
    public $itemCategories;
    /** @var  \App\BackOfficeModule\Factories\AddItemFormFactory @inject */
    public $addItemFactory;

    public function renderDefault(){
        $this->template->post_max_size = (int)(str_replace('M', '', ini_get('post_max_size')) * 1024 * 1024);
        $this->template->menuCategories = $this->itemCategories->getMenu();
    }

    protected function createComponentAddItem(){
        $form = $this->addItemFactory->create();
        $form->onSuccess[] = function(Form $form, $values){
            //$this->redirect(":Homepage:product", ["item" => Strings::webalize($values->name), "itemId" => $this->addItemFactory->getItemId()]);
        };
        return$form;

    }
}