<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 24.09.2017
 * Time: 14:49
 */

namespace App\BackOfficeModule\Presenters;

use Nette;
use App\Presenters\BasePresenter as MainBasePresenter;

abstract class BasePresenter extends MainBasePresenter
{
    public $basePath;
    /** @var  \App\BackOfficeModule\Model\Facades\OrdersFacade @inject */
    public $ordersFacade;

    public function startup(){
        parent::startup();

        if (!$this->user->isAllowed("backOffice")) {
            //$this->redirect(":Homepage:");
            $this->redirect("Login:");
        }

        $this->basePath = rtrim($this->template->baseUri, '/');

        $this->template->adminName = $this->user->getIdentity()->name;
        $this->template->adminSurname = $this->user->getIdentity()->surname;
    }

    public function handleLogoutUser(){
        $this->user->logout();
        $this->redirect(":Homepage:");
    }
}