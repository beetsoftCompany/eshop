<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 04.10.2017
 * Time: 18:12
 */

namespace App\BackOfficeModule\Presenters;

use Nette;
use Nette\Application\UI\Form;
use App\Presenters\BasePresenter as MainBasePresenter;

class LoginPresenter extends MainBasePresenter
{
    public function startup() {
        parent::startup();
        if ($this->user->isAllowed("backOffice")) {
            $this->redirect("Homepage:");
        }
    }

    public function renderDefault(){

    }

    public function createComponentLoginForm(){
        $form = new Form();
        $form->addText("username", "Login: ");
        $form->addPassword("password", "Heslo: ");
        $form->addSubmit("submit", "Přihlásit se");
        $form->onSuccess[] = function(Form $form, $values){
            try{
                $this->getUser()->login($values->username, $values->password);
                $this->redirect("Orders:");
            }
            catch(Nette\Security\AuthenticationException $e){
                $this->flashMessage($e->getMessage());
            }
        };
        return $form;
    }
}