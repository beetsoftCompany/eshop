<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 04.10.2017
 * Time: 18:12
 */

namespace App\BackOfficeModule\Presenters;

use App\Models\Entities\Items;
use Nette;
use App\Models\Entities\ItemsVisits;

class StatisticsPresenter extends BasePresenter
{
    /** @var  \App\BackOfficeModule\Model\StatisticsInfo @inject */
    public $statisticsInfo;

    public function renderDefault(){
        $this->template->mostVisitedItems = $this->EntityManager->getRepository(ItemsVisits::class)->getMostVisitedItems();
        $this->template->bestSellingProducts = $this->EntityManager->getRepository(Items::class)->findBestSellingProducts();
        $this->template->todayOrders = $this->ordersFacade->todayOrdersCount();
        $this->template->mostVisitedOs = $this->EntityManager->getRepository(ItemsVisits::class)->getMostUsedOS();
        $this->template->mostVisitedBrowsers = $this->EntityManager->getRepository(ItemsVisits::class)->getMostUsedBrowsers();
    }
}