<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 29.11.2017
 * Time: 17:50
 */

namespace App\BackOfficeModule\Factories;

use Nette;
use Nette\Application\UI\Form;
use Doctrine\ORM\EntityManager;
use App\Models\Entities\PaymentMethods;

class AddPaymentMethodFormFactory
{
    /** @var EntityManager  */
    private $entityManager;

    function __construct(EntityManager $entityManager){
        $this->entityManager = $entityManager;
    }

    public function create(){
        $form = new Form();
        $form->addText("name", "Název položky:")
            ->setRequired("Vyplňte toto pole");
        $form->addInteger("price", "Cena:")
            ->setRequired("Vyplňte toto pole");
        $form->addSubmit('submit', 'Přidat');
        $form->onSuccess[] = [$this, 'formSucceeded'];
        return $form;
    }

    public function formSucceeded(Form $form, $values){
        $paymentMethod = new PaymentMethods();
        $paymentMethod->name = $values->name;
        $paymentMethod->price = $values->price;
        $this->entityManager->persist($paymentMethod);
        $this->entityManager->flush();
    }
}