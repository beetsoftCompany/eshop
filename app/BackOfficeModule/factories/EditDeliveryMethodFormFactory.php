<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 29.11.2017
 * Time: 17:50
 */

namespace App\BackOfficeModule\Factories;

use App\Models\Entities\PaymentMethodsOfDeliveryMethods;
use Nette;
use Nette\Application\UI\Form;
use Doctrine\ORM\EntityManager;
use App\Models\Entities\DeliveryMethods;
use App\Models\Entities\PaymentMethods;

class EditDeliveryMethodFormFactory
{
    /** @var EntityManager  */
    private $entityManager;

    function __construct(EntityManager $entityManager){
        $this->entityManager = $entityManager;
    }

    public function create(){
        $form = new Form();
        $form->addHidden("methodId")
            ->setRequired("Vyplňte toto pole");
        $form->addText("name", "Název položky:")
            ->setRequired("Vyplňte toto pole");
        $form->addInteger("price", "Cena:")
            ->setRequired("Vyplňte toto pole");
        $form->addCheckboxList("paymentMethods", "Podporované platební metody:",  $this->entityManager->getRepository(PaymentMethods::class)->findPaymentMethodsForCheckbox());
        $form->addSubmit('submit', 'Přidat');
        $form->onSuccess[] = [$this, 'formSucceeded'];
        return $form;
    }

    public function formSucceeded(Form $form, $values){
        $deliveryMethod = $this->entityManager->find(DeliveryMethods::class, $values->methodId);
        $deliveryMethod->setName($values->name);
        $deliveryMethod->setPrice($values->price);
        $this->entityManager->persist($deliveryMethod);
        $this->entityManager->flush();
        $this->entityManager->remove($this->entityManager->getRepository(PaymentMethodsOfDeliveryMethods::class)->findByDeliveryMethodId($values->methodId));
        $this->entityManager->flush();
        foreach($values->paymentMethods as $paymentMethod){
            $paymentMethodOfDeliveryMethod = new PaymentMethodsOfDeliveryMethods();
            $paymentMethodOfDeliveryMethod->setDeliveryMethodId($values->methodId);
            $paymentMethodOfDeliveryMethod->setPaymentMethodId($paymentMethod);
            $this->entityManager->persist($paymentMethodOfDeliveryMethod);
        }
        $this->entityManager->flush();
    }
}