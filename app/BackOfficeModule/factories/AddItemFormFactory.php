<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 29.11.2017
 * Time: 17:50
 */

namespace App\BackOfficeModule\Factories;

use Nette;
use Nette\Application\UI\Form;
use Nette\Utils\Image;
use Nette\Utils\FileSystem;
use Doctrine\ORM\EntityManager;
use App\Models\Entities\Items;
use App\Models\Facades\SettingsFacade;

class AddItemFormFactory
{
    private $entityManager;
    private $imageDir;
    private $itemId;
    private $settingsFacade;

    function __construct($imageDir, EntityManager $entityManager, SettingsFacade $settingsFacade){
        $this->entityManager = $entityManager;
        $this->imageDir = $imageDir;
        $this->settingsFacade = $settingsFacade;
    }

    public function create(){
        $form = new Form();
        $form->addText("name", "Název položky:")
            ->setRequired("Vyplňte toto pole");
        $form->addInteger("price", "Cena:")
            ->setRequired("Vyplňte toto pole");
        $form->addText("availability", "Dostupnost:")
            ->setRequired("Vyplňte toto pole");
        $form->addInteger("quantity", "Množství:")
            ->setRequired("Vyplňte toto pole");
        $form->addMultiUpload("images", "Obrázky:")
            ->addRule(Form::IMAGE, 'Soubor musí být JPEG, PNG nebo GIF.')
            ->setRequired(false);
        $form->addTextArea("shortDescription", "Krátký popis:");
        $form->addTextArea("description", "Popis:");
        $form->addSelect("dph", "Typ DPH:", $this->settingsFacade->getVat())
            ->setRequired("Vyplňte toto pole");
        $form->addHidden("titleImg");
        $form->addSubmit('submit', 'Přidat');
        $form->onSuccess[] = [$this, 'addItemFormSucceeded'];
        return $form;
    }

    public function addItemFormSucceeded(Form $form, $values){
        $item = new Items();
        $item->setName($values->name);
        $item->setPrice($values->price);
        $item->setAvailability($values->availability);
        $item->setQuantity($values->quantity);
        $item->setTitleImage($values->titleImg);
        $item->setDescription($values->description);
        $item->setShortDescription($values->shortDescription);
        $item->setDph($values->dph);
        $this->entityManager->persist($item);
        $this->entityManager->flush();
        $this->itemId = $item->getId();
        foreach($values->images as $image){
            $image->move($this->imageDir . '/photos/' . $this->itemId . '/' . $image->name);

            $imageFile = Image::fromFile($this->imageDir . "\\photos\\" . $this->itemId . "\\" . $image->name);
            if($imageFile->getWidth() > $imageFile->getHeight()) {
                $imageFile->resize(500, NULL);
            }
            else {
                $imageFile->resize(NULL, 500);
            }
            $imageFile->sharpen();
            FileSystem::createDir("$this->imageDir\\thumbs\\$this->itemId");
            $imageFile->save("$this->imageDir\\thumbs\\$this->itemId\\$image->name");
        }
    }
    public function getItemId(){
        return $this->itemId;
    }
}