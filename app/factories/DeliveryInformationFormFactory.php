<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 25.09.2017
 * Time: 18:18
 */

namespace App\Factories;

use App\Models\BaseManager;
use Nette;
use Nette\Application\UI\Form;

class DeliveryInformationFormFactory extends BaseManager
{
    protected $session;

    public function __construct(Nette\Http\Session $session)
    {
        $this->session = $session;
    }

    public function create(){
        $form = new Form;

        $form->addGroup('Kontaktní údaje');
            //->setHtmlAttribute('class', 'orderPersonForm');
        $form->addText("firstname", "Jméno:")
            ->setHtmlAttribute('class', 'orderName')
            ->setRequired("Vyplňte toto pole");
        $form->addText("surname", "Příjmení")
            ->setHtmlAttribute('class', 'orderSurename')
            ->setRequired("Vyplňte toto pole");
        $form->addText("email", "Email:")
            ->setHtmlAttribute('class', 'orderEmail')
            ->setRequired("Vyplňte toto pole")
            ->addRule(Form::EMAIL, "Zadejte správně email!");
        $form->addText("phone", "Telefon")
            ->setHtmlAttribute('class', 'orderPhone')
            ->setRequired("Vyplňte toto pole");

        $form->addGroup('Dodací adresa');
            //->setHtmlAttribute('class', 'orderAddressForm');
        $form->addText("billingStreet", "Ulice a č.p.:")
            ->setRequired("Vyplňte toto pole");
        $form->addText("billingTown", "Obec:")
            ->setRequired("Vyplňte toto pole");
        $form->addText("billingPSC", "PSČ:")
            ->setRequired("Vyplňte toto pole");
        $form->addText("billingState", "Stát:")
            ->setRequired("Vyplňte toto pole");
/*
        $form->addGroup('Fakturační adresa');
        $form->addText("deliveryStreet", "Ulice a č.p.:")
            ->setRequired("Vyplňte toto pole");
        $form->addText("deliveryTown", "Obec:")
            ->setRequired("Vyplňte toto pole");
        $form->addText("deliveryPSC", "PSČ:")
            ->setRequired("Vyplňte toto pole");
        $form->addText("deliveryState", "Stát:")
            ->setRequired("Vyplňte toto pole");*/

        $form->addSubmit('submit', 'Pokračovat v objednávce');
        $form->onSuccess[] = [$this, 'deliveryInformationsControlSucceeded'];
        return $form;
    }

    public function deliveryInformationsControlSucceeded(Form $form, $values){
        $this->session->getSection('orders')->deliveryInformations = (object)[
            "firstname" => $values->firstname,
            "surname" => $values->surname,
            "email" => $values->email,
            "phone" => $values->phone,
            "billingStreet" => $values->billingStreet,
            "billingTown" => $values->billingTown,
            "billingPSC" => $values->billingPSC,
            "billingState" => $values->billingState
        ];
    }
}


/*
        $order->firstname = $values->firstname;
        $order->surname = $values->surname;
        $order->email = $values->email;
        $order->phone = $values->phone;

        $order->billingStreet = $values->billingStreet;
        $order->billingTown = $values->billingTown;
        $order->billingPSC = $values->billingPSC;
        $order->billingState = $values->billingState;
*/