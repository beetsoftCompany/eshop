<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 12.11.2017
 * Time: 11:41
 */

namespace App\Factories\CategoryMenu;

use Nette;
use Nette\Application\UI\Form;
use Nette\Caching\Cache;
use Doctrine\ORM\EntityManager;

use App\Models\Entities\CategoryList;
use App\Models\Entities\CategoryBranching;
use App\Models\ItemCategories;

class DeleteCategoryFormFactory
{
    /** @var EntityManager  */
    private $entityManager;
    /** @var Cache  */
    private $cache;
    /** @var ItemCategories  */
    private $itemCategories;

    function __construct(EntityManager $entityManager, ItemCategories $itemCategories){
        $this->entityManager = $entityManager;
        $this->itemCategories = $itemCategories;

        $storage = new Nette\Caching\Storages\FileStorage(__DIR__ . '/../../../temp/cache');
        $this->cache = new Cache($storage);
    }

    public function create(){
        $form = new Form;
        $form->addHidden('categoryId');
        $form->addSubmit('submit', 'Smazat');
        $form->onSuccess[] = [$this, 'deleteCategoryFormSucceeded'];
        return $form;
    }

    public function deleteCategoryFormSucceeded(Form $form, $values){
        $this->entityManager->remove($this->entityManager->find(CategoryList::class, $values->categoryId));
        $this->entityManager->remove($this->entityManager
            ->getRepository(CategoryBranching::class)
            ->findByParentId($values->categoryId));
        $this->entityManager->remove($this->entityManager
            ->getRepository(CategoryBranching::class)
            ->findByChildId($values->categoryId));
        $this->entityManager->flush();

        $this->cache->save("categoryMenu", $this->itemCategories->getMenu());
    }
}