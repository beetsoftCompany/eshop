<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 24.09.2017
 * Time: 14:48
 */

namespace App\OrderModule\Presenters;

use Nette;
use Nette\Application\UI\Form;
use App\Presenters\BasePresenter;
use App\Models\Entities\Orders;
use App\Models\Entities\OrderItems;
use App\Models\Entities\DeliveryMethods;
use App\Models\Entities\PaymentMethods;
use App\Models\Entities\Items;


class OrderPresenter extends BasePresenter
{
    public function renderDefault()
    {
        $this->redirect("Order:step1");
    }

    public function renderStep1()
    {
        $orders = $this->getOrders();
        //$this->template->orders = $orders;
        $this->template->totalPrice = $this->getTotalPrice($orders);
    }
    public function renderStep2(){
        if ($this->getSession()->getSection('orders')->cart != null && $this->getSession()->getSection('orders')->cart != []) {
            //$this->template->deliveryMethods = $this->deliveryAndPaymentMethods->getDeliveryMethods();
            //$this->template->paymentMethods = $this->deliveryAndPaymentMethods->getPaymentMethods();

            /** Nevím k čemu to je  */
            $this->template->deliveryMethods = $this->EntityManager->getRepository(DeliveryMethods::getClassName())->findAll();
            $this->template->paymentMethods = $this->EntityManager->getRepository(PaymentMethods::getClassName())->findAll();
        }
        else{
            $this->redirect("Order:step1");
        }
    }
    public function renderStep3(){
        $sectionOrders = $this->getSession()->getSection('orders');
        if ($sectionOrders->cart != null && $sectionOrders->cart != [] && $sectionOrders->deliveryMethod != null && $sectionOrders->paymentMethod != null) {

        }
        else{
            $this->redirect("Order:step1");
        }
    }
    public function renderStep4(){
        $sectionOrders = $this->getSession()->getSection('orders');
        if ($sectionOrders->cart != null && $sectionOrders->cart != [] && $sectionOrders->deliveryMethod != null && $sectionOrders->paymentMethod != null) {
            var_dump($sectionOrders->deliveryInformations);
            var_dump($sectionOrders->deliveryMethod);
            var_dump($sectionOrders->paymentMethod);
        }
        else{
            $this->redirect("Order:step1");
        }
    }
    public function handleChangeOrderQuantity($itemId, $quantity = 0)
    {
        if ($this->isAjax()) {
            $this->changeOrderQuantity($itemId, $quantity, true);

            $orders = $this->getOrders();
            $this->template->orders = $orders;
            $this->template->totalPrice = $this->getTotalPrice($orders);
            $this->redrawControl('ordersList');

        } else {
            $this->redirect("this");
        }
    }

    protected function createComponentDeliveryAndPaymentMethods(){
        $form = new Form;

        $deliveryMethodsArray = [];
        $paymentMethodsArray = [];

        $deliveryMethods = $this->EntityManager->getRepository(DeliveryMethods::getClassName())->findAll();
        $paymentMethods = $this->EntityManager->getRepository(PaymentMethods::getClassName())->findAll();

        //foreach($this->deliveryAndPaymentMethods->getDeliveryMethods() as $method){
        foreach($deliveryMethods as $method){
            //$deliveryMethods[$method->id] = ["name" => $method->method, "price" => $method->price];
            $deliveryMethodsArray[$method->id] = $method->name . " Cena: " . $method->price . " Kč";
        }
        //foreach($this->deliveryAndPaymentMethods->getPaymentMethods() as $method){
        foreach($paymentMethods as $method){
            $paymentMethodsArray[$method->id] = $method->name . " Cena: " . $method->price . " Kč";
        }

        $form->addRadioList('deliveryMethods', 'Způsob dodání:', $deliveryMethodsArray);
        $form->addRadioList('paymentMethods', 'Způsob platby:', $paymentMethodsArray);
        $form->addSubmit('deliveryAndPaymentMethodsSubmit', 'Pokračovat v objednávce')
            ->setAttribute('class', 'ui-button ui-widget ui-corner-all');
        $form->onSuccess[] = [$this, 'deliveryAndPaymentMethodsSucceeded'];
        return $form;
    }
    protected function createComponentDeliveryInformations(){
        $form = $this->deliveryInformationFormFactory->create();
        $form->onSuccess[] = function(){
            $this->redirect("Order:step4");
        };
        return $form;
    }
    protected function createComponentConfirmOrder(){
        $form = new Form();
        $form->addSubmit("confirmOrder", "Dokončit objednávku");
        $form->onSuccess[] = function(){
            $orderSection = $this->session->getSection('orders');

            $orderItems = $orderSection->cart;
            $deliveryMethod = $orderSection->deliveryMethod;
            $paymentMethod = $orderSection->paymentMethod;

            $deliveryInformations = $orderSection->deliveryInformations;

            //$this->order->order($orderItems, $deliveryInformations->firstname, $deliveryInformations->surname, $deliveryInformations->email, $deliveryInformations->phone, $deliveryInformations->billingStreet, $deliveryInformations->billingTown, $deliveryInformations->billingPSC, $deliveryInformations->billingState, $deliveryInformations->billingStreet, $deliveryInformations->billingTown, $deliveryInformations->billingPSC, $deliveryInformations->billingState, $deliveryMethod, $paymentMethod);
            $order = new Orders();
            $order->setFirstname($deliveryInformations->firstname);
            $order->setSurname($deliveryInformations->surname);
            $order->setEmail($deliveryInformations->email);
            $order->setPhone($deliveryInformations->phone);
            $order->setBillingStreet($deliveryInformations->billingStreet);
            $order->setBillingTown($deliveryInformations->billingTown);
            $order->setBillingPsc($deliveryInformations->billingPSC);
            $order->setBillingState($deliveryInformations->billingState);
            $order->setDeliveryStreet($deliveryInformations->billingStreet);
            $order->setDeliveryTown($deliveryInformations->billingTown);
            $order->setDeliveryPsc($deliveryInformations->billingPSC);
            $order->setDeliveryState($deliveryInformations->billingState);
            $order->setDeliveryMethod($deliveryMethod);
            $order->setPaymentMethod($paymentMethod);
            $this->EntityManager->persist($order);
            $this->EntityManager->flush();
            $orderId = $order->getId();

            foreach($orderItems as $itemId => $itemQuantity){
                $orderItems = new OrderItems();
                $orderItems->setOrderId($orderId);
                $orderItems->setItem($itemId);
                $orderItems->setQuantity($itemQuantity);
                $this->EntityManager->persist($orderItems);
                $updateQ = $this->EntityManager->createQuery("UPDATE " . Items::getClassName() ." i SET i.countOfPurchases = i.countOfPurchases + :quantity, i.quantity = i.quantity - :quantity WHERE i.id = :id");
                $updateQ->setParameter('quantity', $itemQuantity);
                $updateQ->setParameter('id', $itemId);
                $updateQ->execute();
            }
            $this->EntityManager->flush();

            unset($orderSection->cart);
            unset($orderSection->deliveryMethod);
            unset($orderSection->paymentMethod);
            unset($paymentMethod);

            $this->redirect(":Homepage:printProducts");
        };
        return $form;
    }

    public function deliveryAndPaymentMethodsSucceeded(Form $form, $values){
        //if($this->deliveryAndPaymentMethods->checkIfThePaymentIsSupported($values->paymentMethods, $values->paymentMethods)){
        if($this->paymentMethodsOfDeliveryMethodsFacade->checkIfThePaymentIsSupported($values->paymentMethods, $values->deliveryMethods)){
            $this->getSession()->getSection('orders')->deliveryMethod = $values->deliveryMethods;
            $this->getSession()->getSection('orders')->paymentMethod = $values->paymentMethods;

            $this->redirect("Order:step3");
        }
        else{
            $this->redirect("this");
        }
    }

    /**
     * @param $orders
     * @return int
     */
    public function getTotalPrice($orders)
    {
        $totalPrice = 0;
        foreach ($orders as $order) {
            $totalPrice += $order['price'] * $order['quantity'];
        }
        return $totalPrice;
    }
}