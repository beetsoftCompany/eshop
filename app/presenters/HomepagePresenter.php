<?php

namespace App\Presenters;

use App\Models\Entities\CategoryBranching;
use Nette;
use Nette\Utils\Strings;
use App\Models\Entities\CategoryContent;
use App\Models\Entities\ItemsVisits;

class HomepagePresenter extends BasePresenter
{
    /** @persistent */
    public $categoryPath;
    /** @persistent */
    public $categoryId;

    /** @persistent */
    public $itemId;

    public function renderPrintProducts(){
        if(!$this->isAjax()) {
            if($this->getHttpRequest()->getQuery("sort")){
                $sort = $this->getHttpRequest()->getQuery("sort");
                $this->template->currentSort = $sort;
            }
            else{
                $this->template->currentSort = "";
                $sort = null;
            }
            $items = $this->EntityManager->getRepository(CategoryContent::class)->getItemsInCategory($this->categoryId, null, null, $sort);
            $this->template->items = $items->items;
            $this->template->minPrice = $items->minPrice;
            $this->template->maxPrice = $items->maxPrice;
            $this->template->categories = $this->itemCategories->createArrayMenu($this->EntityManager->getRepository(CategoryBranching::class)->getCategoryBranching(), 1, $this->categoryId);
        }
    }
    public function renderProduct(){
        if(!$this->isAjax()) {
            $item = $this->itemsFacade->getItem($this->itemId);
            if(!$item) {
                $this->error();
            }
            $this->template->dph = $this->settingsFacade->getSetting($item->dph);
            $this->template->item = $item;
            $this->itemsFacade->getItemImages($this->itemId);

            $this->statistics->itemVisit($item->id);
        }
    }
    public function renderTest(){
        //dump($this->link("Homepage:printProducts", ["categoryPath" => "aaa", "categoryId" => "3"]));
        //dump($this->link("Homepage:product", ["item" => "aaa", "itemId" => "3"]));die();
    }

    public function handleGetSearchResult($text){
        if($this->isAjax()){
            $this->payload->results = $this->searchEngine->getResults($text);
            $this->payload->test = $text;
            $this->sendPayload();
        }
        else{
            $this->redirect("this");
        }
    }
    public function handleGetUrl($type, $selectedId){
        if($this->isAjax()){
            if($type == 'category'){
                $categoryName = str_replace('-', '_', Strings::webalize($this->categoryListFacade->getCategory($selectedId)->name));
                $this->payload->url = $this->link("Homepage:printProducts", ["categoryPath" => $categoryName, "categoryId" => $selectedId]);
            }
            elseif ($type == 'product'){
                $itemsName = str_replace('-', '_', Strings::webalize($this->itemsFacade->getItem($selectedId)->name));
                $this->payload->url = $this->link("Homepage:product", ["item" => $itemsName, "itemId" => $selectedId]);
            }
            $this->sendPayload();
        }
        else{
            $this->redirect("this");
        }
    }

    public function handleAddToCart($itemId, $quantity = 0){
        if($this->isAjax()){
            $this->changeOrderQuantity($itemId, $quantity);
            $this->template->orders = $this->getOrders();
            $this->payload->message = $this->translator->translate('cart.addedToTheCart');
            $this->redrawControl("ordersList");
        }
        else{
            $this->redirect("this");
        }
    }
    public function handleOrderList(){
        if($this->isAjax()){
            $this->template->orders =  $this->getSession()->getSection('orders')->cart;
            $this->redrawControl("ordersList");
        }
        else{
            $this->redirect("this");
        }
    }
    public function handleChangeSort($sortType, $minPrice, $maxPrice, $currentSort = null){
        if($this->isAjax()){
            if($sortType == "priceRange"){
                $items = $this->EntityManager->getRepository(CategoryContent::class)->getItemsInCategory($this->categoryId, $minPrice, $maxPrice, $currentSort);
                $sortType = $currentSort;
            }
            else{
                $items = $this->EntityManager->getRepository(CategoryContent::class)->getItemsInCategory($this->categoryId, $minPrice, $maxPrice, $sortType);
            }
            $this->template->items = $items->items;
            $this->template->minPrice = $minPrice;
            $this->template->maxPrice = $maxPrice;
            $this->template->currentSort = $sortType;
            $this->redrawControl("itemList");
        }
        else{
            $this->redirect("this");
        }
    }
}
