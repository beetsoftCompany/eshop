<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 20.12.2017
 * Time: 16:48
 */

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;

class LoginPresenter extends BasePresenter
{
    public function renderDefault(){

    }

    public function createComponentLoginForm(){
        $form = new Form();
        $form->addText("username", "Login: ");
        $form->addPassword("password", "Heslo: ");
        $form->addSubmit("submit", "Přihlásit se");
        $form->onSuccess[] = function(Form $form, $values){
            try{
                $this->getUser()->login($values->username, $values->password);
                $this->redirect("BackOffice:Orders:");
            }
            catch(Nette\Security\AuthenticationException $e){
                $this->flashMessage($e->getMessage());
            }
        };
        return $form;
    }
}