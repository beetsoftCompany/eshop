<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 24.09.2017
 * Time: 14:49
 */

namespace App\Presenters;

use App\Models\Facades\PaymentMethodsFacade;
use App\Models\Statistics;
use Nette;
use App\Components\MenuControl;

abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    /** @persistent */
    public $locale;

    /** @var \Kdyby\Doctrine\EntityManager @inject*/
    public $EntityManager;

    /** @var \Kdyby\Translation\Translator @inject */
    public $translator;

    /** @var \App\Models\Facades\ItemsFacade @inject */
    public $itemsFacade;
    /** @var  \App\Models\Facades\SettingsFacade @inject */
    public $settingsFacade;
    /** @var  \App\Models\Facades\PaymentMethodsFacade @inject*/
    public $paymentMethodsFacade;
    /** @var \App\Models\Facades\PaymentMethodsOfDeliveryMethodsFacade @inject */
    public $paymentMethodsOfDeliveryMethodsFacade;
    /** @var  \App\Models\Facades\CategoryListFacade @inject */
    public $categoryListFacade;

    /** @var  \App\Components\Menu\IMenuControl @inject */
    public $menuControl;

    /** @var \App\Factories\DeliveryInformationFormFactory @inject */
    public $deliveryInformationFormFactory;
    /** @var  \App\Models\SearchEngine @inject */
    public $searchEngine;
    /** @var  \App\Models\Statistics @inject */
    public $statistics;
    /** @var  \App\Models\ItemCategories @inject */
    public $itemCategories;

    public function startup()
    {
        parent::startup();
        $this->template->orders = $this->getOrders();
        $this->template->totalPrice = $this->getTotalPrice($this->template->orders);
        
        if(!$this->isAjax()){
            $this->template->orders = $this->getOrders();
        }
    }

    /**
     * @param null $maxLevel
     * @param null $isAdmin
     * @return \App\Components\Menu\MenuControl
     */
    protected function createComponentCategoriesMenu($maxLevel = null, $isAdmin = null)
    {
        $control = $this->menuControl->create($maxLevel, $isAdmin);
        return $control;
    }

    public function handleEmptyCart()
    {
        if ($this->isAjax()) {
            $this->getSession()->getSection('orders')->cart = [];
            $this->payload->message = $this->translator->translate('cart.emptiedCart');//"Košík byl vysypán.";
            $this->template->orders = [];
            $this->redrawControl("ordersList");
        } else {
            $this->redirect("this"); 
        }
    }

    public function handleDeleteFromCart($itemId)
    {
        if ($this->isAjax()) {
            unset($this->getSession()->getSection('orders')->cart[$itemId]);
            $this->template->orders = $this->getOrders();
            $this->redrawControl("ordersList");
        } else {
            $this->redirect("this");
        }
    }
    
    /**
     * @return array
     */
    protected function getOrders(){
        $orderIdes = $this->getSession()->getSection('orders')->cart;
        $orderItemsArray = [];
        if (!empty($orderIdes)) {
            foreach ($orderIdes as $itemId => $quantity) {
                $itemArray = [];
                //$item = $this->items->getItemById($itemId);
                $item = $this->itemsFacade->getItem($itemId);

                $itemArray['name'] = $item->name;
                $itemArray['quantity'] = $quantity;
                $itemArray['id'] = $itemId;
                $itemArray['price'] = $item->price;
                $itemArray['priceString'] = number_format($itemArray['price'], 2, ',', ' ');
                $itemArray['totalPrice'] = $item->price * $quantity;
                $itemArray['totalPriceString'] = number_format($itemArray['totalPrice'], 2, ',', ' ');
                $itemArray['availability'] = $item->availability;
                $itemArray['titleImage'] = $item->titleImage;
                $orderItemsArray[] = $itemArray;
            }
        }
        return $orderItemsArray;
    }

    /**
     * @param $itemId
     * @param int $quantity
     * @param bool $totalChange
     */
    protected function changeOrderQuantity($itemId, $quantity = 0, $totalChange = false){
        if (gettype($quantity) == "integer" && $quantity > 0) {
            if ($this->getSession()->getSection('orders')->cart == null) {
                $this->getSession()->getSection('orders')->cart = [];
            }

            if (array_key_exists($itemId, $this->getSession()->getSection('orders')->cart) && $totalChange == false) {
                $this->getSession()->getSection('orders')->cart[$itemId] = $this->getSession()->getSection('orders')->cart[$itemId] + $quantity;
            } else {
                $this->getSession()->getSection('orders')->cart[$itemId] = (int)$quantity;
            }
        }
    }
    
    public function getTotalPrice($orders)
    {
    	$totalPrice = 0;
    	foreach ($orders as $order) {
    		$totalPrice += $order['price'] * $order['quantity'];
    	}
    	return $totalPrice;
    }
}