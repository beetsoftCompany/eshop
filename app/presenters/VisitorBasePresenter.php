<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 24.09.2017
 * Time: 14:49
 */

namespace App\Presenters;

use App\Models\Facades\PaymentMethodsFacade;
use Nette;
use App\Components\MenuControl;

abstract class VisitorPresenter extends Nette\Application\UI\Presenter
{
    /** @var \Kdyby\Doctrine\EntityManager @inject*/
    public $EntityManager;

    public function startup(){
        parent::startup();
    }
}