<?php
namespace App\Components\Menu;

/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 04.12.2017
 * Time: 19:32
 */
interface IMenuControl
{
    /** @return MenuControl */
    function create();
}