<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 04.12.2017
 * Time: 19:32
 */

namespace App\Components\Menu;


use App\Factories\CategoryMenu\AddCategoryFormFactory;
use App\Factories\CategoryMenu\DeleteCategoryFormFactory;
use App\Models\ItemCategories;
use Nette\Application\UI\Control;
use Nette\Application\UI\Multiplier;

class MenuControl extends Control
{
    private $itemCategories;
    private $addCategoryFormFactory;
    private $deleteCategoryFormFactory;

	/**
	 * MenuControl constructor.
	 * @param ItemCategories $itemCategories
	 * @param DeleteCategoryFormFactory $deleteCategoryFormFactory
	 */
    public function __construct(ItemCategories $itemCategories, DeleteCategoryFormFactory $deleteCategoryFormFactory){
        parent::__construct();
        $this->itemCategories = $itemCategories;
        //$this->addCategoryFormFactory = $addCategoryFormFactory;
        $this->deleteCategoryFormFactory = $deleteCategoryFormFactory;
    }

    /**
     * @param null $maxLevel
     * @param null $isAdmin
     */
    public function render($maxLevel = null, $isAdmin = null)
    {
        $menu = $this->itemCategories->getMenu($maxLevel);
        $template = $this->template;
        $template->setFile(__DIR__ . '/templates/menuContainer.latte');
        $template->itemCategories = $menu;
        $template->maxLevel = $maxLevel;
        $template->isAdmin = $isAdmin;
        $template->render();
    }

    public function handleDeleteCategory($categoryId){

    }

    /**
     * @return Multiplier
     */
    public function createComponentAddCategoryForm(){
        return new Multiplier(function () {
            $form = $this->addCategoryFormFactory->create();
            $form->onSuccess[] = function(){
                if($this->presenter->isAjax()){
                    $this->redrawControl('categoryMenu');
                }
            };
            return $form;
        });
    }
    public function createComponentDeleteCategoryForm(){
        return new Multiplier(function () {
            $form = $this->deleteCategoryFormFactory->create();
            $form->onSuccess[] = function(){
                if($this->presenter->isAjax()){
                    $this->redrawControl('categoryMenu');
                }
            };
            return $form;
        });
    }
}