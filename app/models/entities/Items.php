<?php

namespace App\Models\Entities;


use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\BaseEntity;

/**
 * @ORM\Entity(repositoryClass="App\Models\Repositories\ItemsRepository")
 */
class Items extends BaseEntity
{
    public function __construct()
    {
        $this->date_of_entry = new \DateTime();
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public $id;
    /**
     * @ORM\Column(type="string")
     */
    public $name;
    /**
     * @ORM\Column(type="float")
     */
    public $price;
    /**
     * @ORM\Column(type="string")
     */
    public $availability;
    /**
     * @ORM\Column(type="integer")
     */
    public $quantity;
    /**
     * @ORM\Column(name="title_image", type="string")
     */
    public $titleImage;
    /**
     * @ORM\Column(type="string")
     */
    public $description;
    /**
     * @ORM\Column(name="short_description", type="string")
     */
    public $shortDescription;
    /**
     * @ORM\Column(name="count_of_purchases", type="integer", options={"default": "0"})
     */
    public $countOfPurchases = 0;
    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    public $date_of_entry;
    /**
     * @ORM\Column(type="string")
     */
    public $dph;
}