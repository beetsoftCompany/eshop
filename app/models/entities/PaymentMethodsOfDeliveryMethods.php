<?php

namespace App\Models\Entities;


use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\BaseEntity;

/**
 * @ORM\Entity(repositoryClass="App\Models\Repositories\PaymentMethodsOfDeliveryMethodsRepository")
 * @ORM\Table(name="payment_methods_of_delivery_methods")
 */
class PaymentMethodsOfDeliveryMethods extends BaseEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public $id;
    /**
     * @ORM\Column(type="integer", name="delivery_method_id")
     */
    public $deliveryMethodId;
    /**
     * @ORM\Column(type="integer", name="payment_method_id")
     */
    public $paymentMethodId;
}