<?php

namespace App\Models\Entities;

use Nette;
use App\Models;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\BaseEntity;

/**
 * @ORM\Entity
 */
class Orders extends BaseEntity
{
    public function __construct()
    {
        $this->date_of_entry = new \DateTime();
    }
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public $id;

    /**
     * @ORM\Column(type="string")
     */
    public $firstname;
    /**
     * @ORM\Column(type="string")
     */
    public $surname;
    /**
     * @ORM\Column(type="string")
     */
    public $email;
    /**
     * @ORM\Column(type="string")
     */
    public $phone;

    /**
     * @ORM\Column(name="billing_street", type="string")
     */
    public $billingStreet;
    /**
     * @ORM\Column(name="billing_town", type="string")
     */
    public $billingTown;
    /**
     * @ORM\Column(name="billing_psc", type="string")
     */
    public $billingPsc;
    /**
     * @ORM\Column(name="billing_state", type="string")
     */
    public $billingState;

    /**
     * @ORM\Column(name="delivery_street", type="string")
     */
    public $deliveryStreet;
    /**
     * @ORM\Column(name="delivery_town", type="string")
     */
    public $deliveryTown;
    /**
     * @ORM\Column(name="delivery_psc", type="string")
     */
    public $deliveryPsc;
    /**
     * @ORM\Column(name="delivery_state", type="string")
     */
    public $deliveryState;

    /**
     * @ORM\Column(name="delivery_method", type="integer")
     */
    public $deliveryMethod;
    /**
     * @ORM\Column(name="payment_method", type="integer")
     */
    public $paymentMethod;
    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    public $date_of_entry;
    /**
     * @ORM\Column(type="integer")
     */
    public $status = 0;
}