<?php

namespace App\Models\Entities;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\BaseEntity;

/**
 * @ORM\Entity(repositoryClass="App\Models\Repositories\ItemsVisitsRepository")
 * @ORM\Table(name="items_visits")
 */
class ItemsVisits extends BaseEntity
{
    public function __construct()
    {
        $this->dateOfVisit = new \DateTime();
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public $id;

    /**
     * @ORM\Column(name="item_id", type="integer")
     */
    public $itemId;

    /**
     * @ORM\Column(name="visitor_ip", type="string")
     */
    public $visitorIp;

    /**
     * @ORM\Column(name="visitor_browser", type="string")
     */
    public $visitorBrowser;

    /**
     * @ORM\Column(name="visitor_os", type="string")
     */
    public $visitorOs;

    /**
     * @ORM\Column(name="date_of_visit", type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    public $dateOfVisit;
}