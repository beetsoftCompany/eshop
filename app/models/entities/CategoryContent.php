<?php

namespace App\Models\Entities;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\BaseEntity;

/**
 * @ORM\Entity(repositoryClass="App\Models\Repositories\CategoryContentRepository")
 * @ORM\Table(name="category_content")
 */
class CategoryContent extends BaseEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public $id;

    /**
     * @ORM\Column(name="category_id", type="integer")
     */
    public $categoryId;

    /**
     * @ORM\Column(name="item_id", type="integer")
     */
    public $itemId;
}