<?php

namespace App\Models\Entities;


use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\BaseEntity;

/**
 * @ORM\Entity(repositoryClass="App\Model\Repositories\OrderItemsRepository")
 */
class OrderItems extends BaseEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;
    /**
     * @ORM\Column(name="order_id", type="integer")
     */
    protected $orderId;
    /**
     * @ORM\Column(type="integer")
     */
    protected $item;
    /**
     * @ORM\Column(type="integer")
     */
    protected $quantity;
}