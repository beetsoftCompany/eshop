<?php

namespace App\Models\Entities;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\BaseEntity;

/**
 * @ORM\Entity(repositoryClass="App\Models\Repositories\CategoryBranchingRepository")
 * @ORM\Table(name="category_branching")
 */
class CategoryBranching extends BaseEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public $id;

    /**
     * @ORM\Column(name="parent_id", type="integer")
     */
    public $parentId;

    /**
     * @ORM\Column(name="child_id", type="integer")
     */
    public $childId;
}