<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 07.01.2018
 * Time: 13:33
 */

namespace App\Models;


use App\Models\Facades\SettingsFacade;
use Nette\SmartObject;
use Nette\Utils\Strings;

class Items
{
	use SmartObject;

    /** @var SettingsFacade  */
    private $settingsFacade;

    function __construct(SettingsFacade $settingsFacade){
        $this->settingsFacade = $settingsFacade;
    }

    /**
     * @param $items
     * @return object
     */
    public function createItemsWithDphValue($items){
        $itemsDynamic = [];

        foreach($items['items'] as $item){
            $item = (array)$item;
            $item['dph'] = (int)$this->settingsFacade->getSetting($item['dph']);
            $item['priceWithDph'] = $item['price'] + $item['price'] * $item['dph'] * 0.01;
            $itemsDynamic[] = (object)$item;
        }
        $items['items'] = $itemsDynamic;
        return (object)$items;
    }

    public function modifyItems($items){
        $itemsArr = (array)$items;
        $itemsDynamic = [];
        foreach($items->items as $item){
            $item = (array)$item;
            $item['urlName'] = Strings::webalize($item['name']);
            $itemsDynamic[] = (object)$item;
        }
        $itemsArr['items'] = $itemsDynamic;
        return (object)$itemsArr;
    }

    /**
     * @param $item
     * @return mixed
     */
    public function getPriceWithDph($item){
        return $item->price + $item->price * $this->settingsFacade->getSetting($item->dph) * 0.01;
    }
}