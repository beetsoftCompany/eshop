<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 15.01.2018
 * Time: 17:49
 */

namespace App\Models\Facades;

use Kdyby\Doctrine\EntityManager;
use App\Models\Entities\CategoryList;
use Nette\SmartObject;

class CategoryListFacade
{
	use SmartObject;

    /** @var EntityManager  */
    private $EntityManager;

    public function __construct(EntityManager $EntityManager)
    {
        $this->EntityManager = $EntityManager;
    }

    public function getCategory($id){
        return isset($id) ? $this->EntityManager->find(CategoryList::getClassName(), $id) : NULL;
    }
}