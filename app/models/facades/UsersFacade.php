<?php

/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 29.10.2017
 * Time: 14:58
 */
namespace App\Models\Facades;

use App\Models\BaseManager;
use App\Models\Entities\Users;

class UsersFacade extends BaseManager
{

    /**
     * @param $id
     * @return null|object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getUser($id){
        return isset($id) ? $this->entityManager->find(Users::class, $id) : NULL;
    }

    /**
     * @param $username
     * @return mixed|null|object
     */
    public function getUserByUsername($username){
        return isset($username) ? $this->entityManager->getRepository(Users::class )->findOneBy(["username" => $username]) : NULL;
    }
}