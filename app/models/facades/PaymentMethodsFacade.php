<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 30.10.2017
 * Time: 13:12
 */

namespace App\Models\Facades;

use Kdyby\Doctrine\EntityManager;
use App\Models\Entities\PaymentMethods;
use Nette\SmartObject;

class PaymentMethodsFacade
{
	use SmartObject;

    /** @var EntityManager  */
    private $EntityManager;

    /** @var PaymentMethodsOfDeliveryMethodsFacade  */
    private $paymentMethodsOfDeliveryMethodsFacade;

    public function __construct(EntityManager $EntityManager, PaymentMethodsOfDeliveryMethodsFacade $paymentMethodsOfDeliveryMethodsFacade)
    {
        $this->EntityManager = $EntityManager;
        $this->paymentMethodsOfDeliveryMethodsFacade = $paymentMethodsOfDeliveryMethodsFacade;
    }
    public function getPaymentMethod($id){
        return isset($id) ? $this->EntityManager->find(PaymentMethods::class, $id) : NULL;
    }

    /**
     * @param $deliveryMethodId
     * @return array
     */
    public function getPaymentMethodsOfDeliveryMethod($deliveryMethodId){
        $allPaymentMethods = $this->EntityManager->getRepository(PaymentMethods::class)->findAll();
        $paymentMethods = [];
        foreach($allPaymentMethods as $paymentMethod){
            $paymentMethodArray = (array)$paymentMethod;
            if($this->paymentMethodsOfDeliveryMethodsFacade->checkIfThePaymentIsSupported($paymentMethod->id, $deliveryMethodId)){
                $paymentMethodArray['isSupported'] = true;
            }
            else{
                $paymentMethodArray['isSupported'] = false;
            }
            $paymentMethods[$paymentMethod->id] = (object)$paymentMethodArray;
        }
        return $paymentMethods;
    }
}