<?php

namespace App\Models\Facades;

use App\Models\Entities\DeliveryMethods;
use Kdyby\Doctrine\EntityManager;
use Nette\SmartObject;

class DeliveryMethodsFacade
{
	use SmartObject;

    /** @var EntityManager  */
    private $EntityManager;

    public function __construct(EntityManager $EntityManager)
    {
        $this->EntityManager = $EntityManager;
    }
    public function getDeliveryMethod($id){
        return isset($id) ? $this->EntityManager->find(DeliveryMethods::getClassName(), $id) : NULL;
    }
    public function getDeliveryMethodWithPaymentMethods($id){

    }
}