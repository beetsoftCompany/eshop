<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 26.11.2017
 * Time: 15:07
 */

namespace App\Models\Facades;

use Kdyby\Doctrine\EntityManager;
use App\Models\Entities\CategoryBranching;
use App\Models\ItemCategories;
use Nette\SmartObject;

class CategoryBranchingFacade
{
	use SmartObject;

    /** @var EntityManager  */
    private $EntityManager;

    /** @var ItemCategories  */
    private $itemCategories;

    public function __construct(EntityManager $EntityManager, ItemCategories $itemCategories)
    {
        $this->EntityManager = $EntityManager;
        $this->itemCategories = $itemCategories;
    }

    public function getSubcategories($categoryId){
        //if($categoryId != null){
            $categories = $this->EntityManager->getRepository(CategoryBranching::class)->getCategoryBranching();
            return $this->itemCategories->getAllSubcategories($categories, $categoryId);
        //}
        //else{
        //    return $this->EntityManager->getRepository(Items::class)->getAllItemsIdes();
        //}
    }
}