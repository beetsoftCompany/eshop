<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 29.10.2017
 * Time: 18:50
 */

namespace App\Models\Facades;


use App\Models\Entities\Settings;
use Kdyby\Doctrine\EntityManager;
use Nette\SmartObject;

class SettingsFacade
{
	use SmartObject;

    private $EntityManager;

    public function __construct(EntityManager $EntityManager)
    {
        $this->EntityManager = $EntityManager;
    }
    public function getSetting($setting){
        return isset($setting) ? $this->EntityManager->getRepository(Settings::class)->findOneByOptionName($setting)->optionValue : NULL;
    }
    public function getVat(){
        $vats = $this->EntityManager->createQueryBuilder()
            ->select('s')
            ->from(Settings::class, "s")
            ->where("s.optionName LIKE 'dph-%'")
            ->getQuery()
            ->getResult();
        $vatsArray = [];
        foreach($vats as $vat){
            $vatsArray[$vat->optionName] = $vat->optionValue . "%";
        }
        return $vatsArray;
    }
}