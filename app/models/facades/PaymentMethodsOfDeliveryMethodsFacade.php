<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 29.10.2017
 * Time: 18:08
 */

namespace App\Models\Facades;


use Kdyby\Doctrine\EntityManager;
use App\Models\Entities\PaymentMethodsOfDeliveryMethods;
use Nette\SmartObject;

class PaymentMethodsOfDeliveryMethodsFacade
{
	use SmartObject;

    private $EntityManager;

    public function __construct(EntityManager $EntityManager)
    {
        $this->EntityManager = $EntityManager;
    }

    /**
     * @param $paymentMethodId
     * @param $deliveryMethodId
     * @return bool|mixed|null|object
     */
    public function checkIfThePaymentIsSupported($paymentMethodId, $deliveryMethodId){
        return isset($paymentMethodId) ? $this->EntityManager->getRepository(PaymentMethodsOfDeliveryMethods::class)->findOneBy(["deliveryMethodId" => $deliveryMethodId, "paymentMethodId" => $paymentMethodId]) : NULL;
    }
}