<?php

namespace App\Models\Facades;

use Kdyby\Doctrine\EntityManager;
use App\Models\Entities\CategoryContent;
use App\Models\Entities\Items;
use Nette\SmartObject;

class CategoryContentFacade
{
	use SmartObject;

    /** @var EntityManager  */
    private $EntityManager;

    /** @var CategoryBranchingFacade  */
    private $categoryBranchingFacade;

    public function __construct(EntityManager $EntityManager, CategoryBranchingFacade $categoryBranchingFacade)
    {
        $this->EntityManager = $EntityManager;
        $this->categoryBranchingFacade = $categoryBranchingFacade;
    }


    public function getMinPrice($categoryId, $categories = null)
    {
        if(!$categories)
            $categories = $this->categoryBranchingFacade->getSubcategories($categoryId);
        return $this->EntityManager->createQueryBuilder()
            ->select('MIN(i.price)')
            ->from(CategoryContent::class, 'cc')
            ->join(Items::class, 'i', 'WITH', 'cc.itemId = i')
            //->where('cc.categoryId = :categoryId')
            ->where('cc.categoryId IN (:categories)')
            //->setParameter('categoryId', $categoryId)
            ->setParameter('categories', $categories)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getMaxPrice($categoryId, $categories = null)
    {
        if(!$categories)
            $categories = $this->categoryBranchingFacade->getSubcategories($categoryId);
        return $this->EntityManager->createQueryBuilder()
            ->select('MAX(i.price)')
            ->from(CategoryContent::class, 'cc')
            ->join(Items::class, 'i', 'WITH', 'cc.itemId = i')
            //->where('cc.categoryId = :categoryId')
            ->where('cc.categoryId IN (:categories)')
            //->setParameter('categoryId', $categoryId)
            ->setParameter('categories', $categories)
            ->getQuery()
            ->getSingleScalarResult();
    }
}