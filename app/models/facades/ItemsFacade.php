<?php

/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 29.10.2017
 * Time: 14:58
 */
namespace App\Models\Facades;

use Nette\SmartObject;
use Nette\Utils\Finder;
use Kdyby\Doctrine\EntityManager;
use App\Models\Entities\Items;

class ItemsFacade
{
	use SmartObject;

    private $EntityManager;
    private $imageDir;

    public function __construct($imageDir, EntityManager $EntityManager)
    {
        $this->EntityManager = $EntityManager;
        $this->imageDir = $imageDir;
    }

    /**
     * @param $id
     * @return array|null|object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getItem($id){
        $item = isset($id) ? $this->EntityManager->find(Items::getClassName(), $id) : NULL;
        if($item !== null){
            $item = (array)$item;
            $item['images'] = $this->getItemImages($id);
            
            if(!file_exists($this->imageDir . '\\photos\\' . $id . '\\' . $item['titleImage'])){
                $item['titleImage'] = null;
            }
            $item = (object)$item;
        }
        return $item;
    }
    public function getItemImages($id){
        $images = [];
        try {
            foreach (Finder::findFiles('')->in($this->imageDir . '\\photos\\' . $id . '\\') as $key => $file) {
                $images[$key] = $file->getFilename();
            }
        }
        catch(\UnexpectedValueException $ex){
            $images = null;
        }
        return $images;
    }
}