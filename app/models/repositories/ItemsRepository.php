<?php

/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 29.10.2017
 * Time: 16:04
 */
namespace App\Models\Repositories;

use Kdyby\Doctrine\EntityRepository;
use Doctrine\ORM\Mapping;
use App\Models\Entities\Items as ItemsEntity;
use App\Models\Facades\SettingsFacade;
use App\Models\Items;

class ItemsRepository extends EntityRepository
{
    /** @var Items  */
    private $items;

    public function __construct($em, Mapping\ClassMetadata $class)
    {
        parent::__construct($em, $class);
        $this->items = new Items(new SettingsFacade($em));
    }

    /**
     * @return array
     */
    public function getAllItemsIdes(){
        $entityManager = $this->_em;
        $result = $entityManager->createQueryBuilder()
            ->select('i.id')
            ->from(ItemsEntity::class, 'i')
            ->getQuery()
            ->getResult();
        return array_map('current', $result);
    }

    /**
     * @param $word
     * @param null $minPrice
     * @param null $maxPrice
     * @param null $order
     * @return object
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findItems($word, $minPrice = null, $maxPrice = null, $order = null){
        $entityManager = $this->_em;
        $items = $entityManager->createQueryBuilder()
            ->select('MIN(i.price) as minPrice', 'MAX(i.price) as maxPrice')
            //->select('i')
            ->from(ItemsEntity::class, "i")
            ->where("i.name LIKE :word")
            //->andWhere("i.price <= :maxPrice")
            ->setParameter("word", "%".$word."%")
            //->setParameter("maxPrice", "4000")
            ->getQuery()
            ->getSingleResult();
        if($minPrice == null){
            $minPrice = $items['minPrice'];
        }
        if($maxPrice == null){
            $maxPrice = $items['maxPrice'];
        }
        if($order == null) {
            $items['items'] = $entityManager->createQueryBuilder()
                ->select('i')
                ->from(ItemsEntity::class, "i")
                ->where("i.name LIKE :word")
                ->andWhere('i.price >= :minPrice AND i.price <= :maxPrice')
                ->setParameter("word", "%" . $word . "%")
                ->setParameter("minPrice", $minPrice)
                ->setParameter("maxPrice", $maxPrice)
                ->getQuery()
                ->getResult();
        }
        elseif($order == "orderFromCheapest"){
            $items['items'] = $entityManager->createQueryBuilder()
                ->select('i')
                ->from(ItemsEntity::class, "i")
                ->where("i.name LIKE :word")
                ->andWhere('i.price >= :minPrice AND i.price <= :maxPrice')
                ->orderBy('i.price')
                ->setParameter("word", "%" . $word . "%")
                ->setParameter("minPrice", $minPrice)
                ->setParameter("maxPrice", $maxPrice)
                ->getQuery()
                ->getResult();
        }
        elseif($order == "orderFromMoreExpensive"){
            $items['items'] = $entityManager->createQueryBuilder()
                ->select('i')
                ->from(ItemsEntity::class, "i")
                ->where("i.name LIKE :word")
                ->andWhere('i.price >= :minPrice AND i.price <= :maxPrice')
                ->orderBy('i.price', 'DESC')
                ->setParameter("word", "%" . $word . "%")
                ->setParameter("minPrice", $minPrice)
                ->setParameter("maxPrice", $maxPrice)
                ->getQuery()
                ->getResult();
        }
        elseif($order == "bestSeller"){
            $items['items'] = $entityManager->createQueryBuilder()
                ->select('i')
                ->from(ItemsEntity::class, "i")
                ->where("i.name LIKE :word")
                ->andWhere('i.price >= :minPrice AND i.price <= :maxPrice')
                ->orderBy('i.countOfPurchases', 'DESC')
                ->setParameter("word", "%" . $word . "%")
                ->setParameter("minPrice", $minPrice)
                ->setParameter("maxPrice", $maxPrice)
                ->getQuery()
                ->getResult();
        }
        elseif($order == "latest"){
            $items['items'] = $entityManager->createQueryBuilder()
                ->select('i')
                ->from(ItemsEntity::class, "i")
                ->where("i.name LIKE :word")
                ->andWhere('i.price >= :minPrice AND i.price <= :maxPrice')
                ->orderBy('i.date_of_entry', 'DESC')
                ->setParameter("word", "%" . $word . "%")
                ->setParameter("minPrice", $minPrice)
                ->setParameter("maxPrice", $maxPrice)
                ->getQuery()
                ->getResult();
        }
        return $this->items->createItemsWithDphValue($items);
    }
    public function findBestSellingProducts(){
        return $this->_em->createQueryBuilder()
            ->select('i')
            ->from(ItemsEntity::class, "i")
            ->orderBy('i.countOfPurchases', 'DESC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }
}