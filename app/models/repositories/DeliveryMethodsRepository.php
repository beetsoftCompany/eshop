<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 22.01.2018
 * Time: 13:57
 */

namespace App\Models\Repositories;

use Kdyby\Doctrine\EntityRepository;
use Kdyby\Doctrine\Mapping;
use App\Models\Entities\DeliveryMethods;
use App\Models\Entities\PaymentMethodsOfDeliveryMethods;
use App\Models\Facades\PaymentMethodsFacade;
use App\Models\Facades\PaymentMethodsOfDeliveryMethodsFacade;

class DeliveryMethodsRepository extends EntityRepository
{
    /** @var PaymentMethodsFacade  */
    private $paymentMethodsFacade;

    public function __construct($em, Mapping\ClassMetadata $class)
    {
        parent::__construct($em, $class);
        $this->paymentMethodsFacade = new PaymentMethodsFacade($em, new PaymentMethodsOfDeliveryMethodsFacade($em));
    }

    public function findDeliveryMethodsWithPaymentMethods(){
        $deliveryMethods = $this->_em->getRepository(DeliveryMethods::class)->findAll();
        $deliveryMethodsWithPaymentMethods = [];
        foreach($deliveryMethods as $deliveryMethod){
            $id = $deliveryMethod->id;
            $paymentMethods = $this->paymentMethodsFacade->getPaymentMethodsOfDeliveryMethod($id);//$this->_em->getRepository(PaymentMethodsOfDeliveryMethods::class)->findPaymentMethodsOfDeliveryMethod($id);
            $deliveryMethod = (array)$deliveryMethod;
            $deliveryMethod['paymentMethods'] = $paymentMethods;
            $deliveryMethodsWithPaymentMethods[$id] = (object)$deliveryMethod;
        }
        return $deliveryMethodsWithPaymentMethods;
    }
}