<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 05.11.2017
 * Time: 14:50
 */

namespace App\Models\Repositories;

use Kdyby\Doctrine\EntityRepository;
use App\Models\Entities\ItemsVisits;
use App\Models\Entities\Items;

class ItemsVisitsRepository extends EntityRepository
{
    public function getMostVisitedItems(){
       $entityManager = $this->_em;
        return $entityManager->createQueryBuilder()
            ->select('i as item', "COUNT(iv) as countOfVisits")
            ->from(ItemsVisits::class, "iv")
            ->join(Items::class, 'i', 'WITH', 'iv.itemId = i')
            ->groupBy("iv.itemId")
            ->orderBy("countOfVisits", "DESC")
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }
    public function getMostUsedOS(){
        return $this->_em->createQueryBuilder()
            ->select('iv.visitorOs as name', 'COUNT(iv) as visitCount')
            ->from(ItemsVisits::class, "iv")
            ->groupBy("iv.visitorOs")
            ->orderBy("visitCount", "DESC")
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }
    public function getMostUsedBrowsers(){
        return $this->_em->createQueryBuilder()
            ->select('iv.visitorBrowser as name', 'COUNT(iv) as visitCount')
            ->from(ItemsVisits::class, "iv")
            ->groupBy("iv.visitorBrowser")
            ->orderBy("visitCount", "DESC")
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }
}