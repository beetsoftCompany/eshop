<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 18.11.2017
 * Time: 12:27
 */

namespace App\Models\Repositories;

use Kdyby\Doctrine\EntityRepository;
use Doctrine\ORM\Mapping;
use App\Models\Entities\Items as ItemsEntity;
use App\Models\Entities\CategoryContent;
use App\Models\Facades\CategoryContentFacade;
use App\Models\Facades\CategoryBranchingFacade;
use App\Models\Facades\SettingsFacade;
use App\Models\ItemCategories;
use App\Models\Items;
use Nette\Utils\Strings;

class CategoryContentRepository extends EntityRepository
{
    /** @var \Doctrine\ORM\EntityManager  */
    private $EntityManager;
    /** @var CategoryBranchingFacade  */
    private $categoryBranchingFacade;
    /** @var CategoryContentFacade  */
    private $categoryContentFacade;
    /** @var   */
    private $itemsFacade;

    public function __construct($em, Mapping\ClassMetadata $class)
    {
        parent::__construct($em, $class);
        $this->EntityManager = $em;
        $this->categoryBranchingFacade = new CategoryBranchingFacade($em, new ItemCategories($em));
        $this->categoryContentFacade = new CategoryContentFacade($em, $this->categoryBranchingFacade);
        $this->itemsFacade = new Items(new SettingsFacade($em));
    }

    public function getItemsInCategory($categoryId, $minPrice = null, $maxPrice = null, $order = null){
        $categories = $this->categoryBranchingFacade->getSubcategories($categoryId);
        $items = [];
        $items['minPrice'] = $this->categoryContentFacade->getMinPrice($categoryId, $categories);
        $items['maxPrice'] = $this->categoryContentFacade->getMaxPrice($categoryId, $categories);

        if($minPrice == null){
            $minPrice = $items['minPrice'];
        }
        if($maxPrice == null){
            $maxPrice = $items['maxPrice'];
        }
/*
        if($order == null){
            $items['items'] = $this->EntityManager->createQueryBuilder()
                ->select('i')
                ->from(CategoryContent::class, 'cc')
                ->join(ItemsEntity::class, 'i', 'WITH', 'cc.itemId = i')
                ->where('i.price >= :minPrice AND i.price <= :maxPrice')
                //->andWhere('cc.categoryId = :categoryId')
                ->andWhere('cc.categoryId IN (:categories)')
                ->setParameter('minPrice', $minPrice)
                ->setParameter('maxPrice', $maxPrice)
                //->setParameter('categoryId', $categoryId)
                ->setParameter('categories', $categories)
                ->getQuery()
                ->getResult();
        }*/
        //elseif($order == "orderFromCheapest"){
        if($order == "orderFromCheapest"){
            $items['items'] = $this->EntityManager->createQueryBuilder()
                ->select('i')
                ->from(CategoryContent::class, 'cc')
                ->join(ItemsEntity::class, 'i', 'WITH', 'cc.itemId = i')
                ->where('i.price >= :minPrice AND i.price <= :maxPrice')
                //->andWhere('cc.categoryId = :categoryId')
                ->andWhere('cc.categoryId IN (:categories)')
                ->orderBy('i.price')
                ->setParameter('minPrice', $minPrice)
                ->setParameter('maxPrice', $maxPrice)
                //->setParameter('categoryId', $categoryId)
                ->setParameter('categories', $categories)
                ->getQuery()
                ->getResult();
        }
        elseif($order == "orderFromMoreExpensive"){
            $items['items'] = $this->EntityManager->createQueryBuilder()
                ->select('i')
                ->from(CategoryContent::class, 'cc')
                ->join(ItemsEntity::class, 'i', 'WITH', 'cc.itemId = i')
                ->where('i.price >= :minPrice AND i.price <= :maxPrice')
                //->andWhere('cc.categoryId = :categoryId')
                ->andWhere('cc.categoryId IN (:categories)')
                ->orderBy('i.price', 'DESC')
                ->setParameter('minPrice', $minPrice)
                ->setParameter('maxPrice', $maxPrice)
                //->setParameter('categoryId', $categoryId)
                ->setParameter('categories', $categories)
                ->getQuery()
                ->getResult();
        }
        elseif($order == "bestSeller"){
            $items['items'] = $this->EntityManager->createQueryBuilder()
                ->select('i')
                ->from(CategoryContent::class, 'cc')
                ->join(ItemsEntity::class, 'i', 'WITH', 'cc.itemId = i')
                ->where('i.price >= :minPrice AND i.price <= :maxPrice')
                //->andWhere('cc.categoryId = :categoryId')
                ->andWhere('cc.categoryId IN (:categories)')
                ->orderBy('i.countOfPurchases', 'DESC')
                ->setParameter('minPrice', $minPrice)
                ->setParameter('maxPrice', $maxPrice)
                //->setParameter('categoryId', $categoryId)
                ->setParameter('categories', $categories)
                ->getQuery()
                ->getResult();
        }
        elseif($order == "latest"){
            $items['items'] = $this->EntityManager->createQueryBuilder()
                ->select('i')
                ->from(CategoryContent::class, 'cc')
                ->join(ItemsEntity::class, 'i', 'WITH', 'cc.itemId = i')
                ->where('i.price >= :minPrice AND i.price <= :maxPrice')
                //->andWhere('cc.categoryId = :categoryId')
                ->andWhere('cc.categoryId IN (:categories)')
                ->orderBy('i.date_of_entry', 'DESC')
                ->setParameter('minPrice', $minPrice)
                ->setParameter('maxPrice', $maxPrice)
                //->setParameter('categoryId', $categoryId)
                ->setParameter('categories', $categories)
                ->getQuery()
                ->getResult();
        }
        else{
            $items['items'] = $this->EntityManager->createQueryBuilder()
                ->select('i')
                ->from(CategoryContent::class, 'cc')
                ->join(ItemsEntity::class, 'i', 'WITH', 'cc.itemId = i')
                ->where('i.price >= :minPrice AND i.price <= :maxPrice')
                //->andWhere('cc.categoryId = :categoryId')
                ->andWhere('cc.categoryId IN (:categories)')
                ->setParameter('minPrice', $minPrice)
                ->setParameter('maxPrice', $maxPrice)
                //->setParameter('categoryId', $categoryId)
                ->setParameter('categories', $categories)
                ->getQuery()
                ->getResult();
        }
        $itemsWithVat =  $this->itemsFacade->createItemsWithDphValue($items);
        return $this->itemsFacade->modifyItems($itemsWithVat);
    }
}