<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 05.11.2017
 * Time: 14:50
 */

namespace App\Models\Repositories;

use Kdyby\Doctrine\EntityRepository;
use App\Models\Entities\CategoryList;

class CategoryListRepository extends EntityRepository
{
    public function findCategories($text){
        return $this->_em->createQueryBuilder()
            ->select('c')
            ->from(CategoryList::class, "c")
            ->where("c.name LIKE :text")
            ->setParameter("text", "%" . $text . "%")
            ->getQuery()
            ->getResult();
    }
}