<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 23.01.2018
 * Time: 17:04
 */

namespace App\Models\Repositories;

use Kdyby\Doctrine\EntityRepository;
use App\Models\Entities\PaymentMethodsOfDeliveryMethods;
use App\Models\Entities\PaymentMethods;

class PaymentMethodsOfDeliveryMethodsRepository extends EntityRepository
{
    //nedodělane
    public function findPaymentMethodsOfDeliveryMethod($deliveryMethodId){
        return $this->_em->createQueryBuilder()
            ->select('pm')
            ->from(PaymentMethodsOfDeliveryMethods::class, "pmodm")
            ->join(PaymentMethods::class, 'pm', 'WITH', 'pmodm.paymentMethodId = pm')
            ->where("pmodm.paymentMethodId = :deliveryMethodId")
            ->setParameter("deliveryMethodId", $deliveryMethodId)
            ->getQuery()
            ->getResult();
    }
}