<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 05.11.2017
 * Time: 14:50
 */

namespace App\Models\Repositories;

use Kdyby\Doctrine\EntityRepository;
use App\Models\Entities\CategoryList;
use App\Models\Entities\CategoryBranching;

class CategoryBranchingRepository extends EntityRepository
{
    public function getCategoryBranching(){
       $entityManager = $this->_em;
        return $entityManager->createQueryBuilder()
            ->select('cb.parentId AS parentId', 'cl.name AS name', 'cl.id AS id')
            ->from(CategoryBranching::class, "cb")
            ->join(CategoryList::class, 'cl', 'WITH', 'cb.childId = cl')
            ->getQuery()
            ->getResult();
    }
}