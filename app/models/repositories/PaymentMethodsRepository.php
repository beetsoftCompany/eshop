<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 22.01.2018
 * Time: 13:57
 */

namespace App\Models\Repositories;

use Kdyby\Doctrine\EntityRepository;
use Kdyby\Doctrine\Mapping;
use App\Models\Entities\PaymentMethods;

class PaymentMethodsRepository extends EntityRepository
{
    public function findPaymentMethodsForCheckbox(){
        $allPaymentMethods = $this->_em->getRepository(PaymentMethods::class)->findAll();
        $paymentMethods = [];
        foreach($allPaymentMethods as $paymentMethod){
            $paymentMethods[$paymentMethod->id] = $paymentMethod->name;
        }
        return $paymentMethods;
    }
}