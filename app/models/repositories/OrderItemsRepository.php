<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 30.10.2017
 * Time: 16:45
 */

namespace App\Model\Repositories;

use Kdyby\Doctrine\EntityRepository;
use App\Models\Entities\Items;
use App\Models\Entities\OrderItems;

class OrderItemsRepository extends EntityRepository
{
    /**
     * @param $orderId
     * @return object
     */
    public function getOrderItems($orderId){
        $entityManager = $this->_em;
        return (object)$entityManager->createQueryBuilder()
            ->select('i AS item', 'oi.quantity')
            ->from(OrderItems::class, "oi")
            ->join(Items::class, 'i', 'WITH', 'oi.item = i.id')
            ->where('oi.orderId = :id')
            ->setParameter('id', $orderId)
            ->getQuery()
            ->getResult();
    }
}