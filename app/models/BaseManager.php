<?php
namespace App\Models;

use Nette;
use Kdyby\Doctrine\EntityManager;
use App\Models\Facades\UsersFacade;

/**
 * Class BaseManager
 * @package App\Models
 */
abstract class BaseManager
{
	use Nette\SmartObject;

    /** @var EntityManager  */
    protected $entityManager;

    /** @var Nette\Http\Session */
    protected $session;

    /**
     * BaseManager constructor.
     * @param EntityManager $entityManager
     * @param Nette\Http\Session $session
     * @param UsersFacade $usersFacade
     */
    public function __construct(EntityManager $entityManager, Nette\Http\Session $session)
    {
        $this->entityManager = $entityManager;
        $this->session = $session;
    }
}