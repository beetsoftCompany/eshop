<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 02.02.2018
 * Time: 17:30
 */

namespace App\Models;

use Nette;
use Nette\Http\Request;
use Doctrine\ORM\EntityManager;
use App\Models\Entities\ItemsVisits;

class Statistics
{
    /** @var EntityManager  */
    private $entityManager;
    /** @var Request  */
    private $httpRequest;

    private $user_agent_LOG;

    function __construct(EntityManager $entityManager, Request $request){
        $this->entityManager = $entityManager;
        $this->httpRequest = $request;
        $this->user_agent_LOG = $_SERVER['HTTP_USER_AGENT'];
    }

    public function itemVisit($itemId){
        $itemVisits = new ItemsVisits();
        $itemVisits->itemId = $itemId;
        $itemVisits->visitorIp = $this->httpRequest->getRemoteAddress();
        $itemVisits->visitorBrowser = $this->getBrowser();
        $itemVisits->visitorOs = $this->getOS();
        $this->entityManager->persist($itemVisits);
        $this->entityManager->flush();
    }

    private function getOS() {

        $os_platform    =   "Unknown OS Platform";

        $os_array       =   array(
            '/windows nt 10/i'     =>  'Windows 10',
            '/windows nt 6.3/i'     =>  'Windows 8.1',
            '/windows nt 6.2/i'     =>  'Windows 8',
            '/windows nt 6.1/i'     =>  'Windows 7',
            '/windows nt 6.0/i'     =>  'Windows Vista',
            '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
            '/windows nt 5.1/i'     =>  'Windows XP',
            '/windows xp/i'         =>  'Windows XP',
            '/windows nt 5.0/i'     =>  'Windows 2000',
            '/windows me/i'         =>  'Windows ME',
            '/win98/i'              =>  'Windows 98',
            '/win95/i'              =>  'Windows 95',
            '/win16/i'              =>  'Windows 3.11',
            '/macintosh|mac os x/i' =>  'Mac OS X',
            '/mac_powerpc/i'        =>  'Mac OS 9',
            '/linux/i'              =>  'Linux',
            '/ubuntu/i'             =>  'Ubuntu',
            '/iphone/i'             =>  'iPhone',
            '/ipod/i'               =>  'iPod',
            '/ipad/i'               =>  'iPad',
            '/android/i'            =>  'Android',
            '/blackberry/i'         =>  'BlackBerry',
            '/webos/i'              =>  'Mobile'
        );

        foreach ($os_array as $regex => $value) {

            if (preg_match($regex, $this->user_agent_LOG)) {
                $os_platform    =   $value;
            }

        }

        return $os_platform;
    }
    private function getBrowser() {
        $browser        =   "Unknown Browser";

        $browser_array  =   array(
            '/msie/i'       =>  'Internet Explorer',
            '/firefox/i'    =>  'Firefox',
            '/safari/i'     =>  'Safari',
            '/chrome/i'     =>  'Chrome',
            '/edge/i'       =>  'Edge',
            '/opera/i'      =>  'Opera',
            '/netscape/i'   =>  'Netscape',
            '/maxthon/i'    =>  'Maxthon',
            '/konqueror/i'  =>  'Konqueror',
            '/mobile/i'     =>  'Handheld Browser'
        );

        foreach ($browser_array as $regex => $value) {

            if (preg_match($regex, $this->user_agent_LOG)) {
                $browser    =   $value;
            }

        }

        return $browser;

    }
}