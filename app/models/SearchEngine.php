<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 07.01.2018
 * Time: 18:16
 */

namespace App\Models;

use Kdyby\Doctrine\EntityManager;
use App\Models\Entities\Items as ItemsEntity;
use App\Models\Entities\CategoryList;
use Nette\SmartObject;

class SearchEngine
{
	use SmartObject;

    /** @var EntityManager  */
    private $entityManager;

    /** @var \App\Models\Items  */
    private $items;

    function __construct(EntityManager $entityManager, Items $items){
        $this->entityManager = $entityManager;
        $this->items = $items;
    }

    /**
     * @param $text
     * @return string
     */
    public function getResults($text){
        $resultItems = $this->entityManager->getRepository(ItemsEntity::class)->findItems($text)->items;
        $resultCategories = $this->entityManager->getRepository(CategoryList::class)->findCategories($text);

        $resultTemp = [];
        if($resultItems !== null){
            foreach($resultItems as $item){
                $resultTemp[] = [
                    "label" => $item->name . " (" . $item->priceWithDph . ",- Kč)",
                    "category" => "Produkty",
                    "type" => "product",
                    "id" => $item->id,
                    "priceWithDph" => $item->priceWithDph
                ];
            }
        }
        if($resultCategories !== null){
            foreach($resultCategories as $category){
                $resultTemp[] = [
                    "label" => $category->name,
                    "category" => "Kategorie",
                    "type" => "category",
                    "id" => $category->id
                ];
            }
        }

        return json_encode($resultTemp);
    }
}