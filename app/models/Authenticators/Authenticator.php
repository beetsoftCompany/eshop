<?php
namespace App\Models\Authenticators;

use Nette\Security as NS;
use App\Models\Facades\UsersFacade;
use Nette\SmartObject;

class Authenticator implements NS\IAuthenticator
{
	use SmartObject;

    /** @var UsersFacade  */
    private $usersFacade;

    /**
     * Authenticator constructor.
     * @param UsersFacade $usersFacade
     */
    function __construct(UsersFacade $usersFacade){
        $this->usersFacade = $usersFacade;
    }

    function authenticate(array $credentials)
    {
        list($username, $password) = $credentials;

        $user = $this->usersFacade->getUserByUsername($username);

        if (!$user) {
            throw new NS\AuthenticationException('Uživatel nenalezen.');
        }

        if (!NS\Passwords::verify($password, $user->password)) {
            throw new NS\AuthenticationException('Špatné heslo.');
        }
        $role = "admin";

        $data = [
            "name" => $user->name,
            "surname" => $user->surname
        ];

        return new NS\Identity($user->id, $role, $data);
    }
}