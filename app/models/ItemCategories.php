<?php
/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 29.09.2017
 * Time: 19:24
 */

namespace App\Models;

use Nette;
use Nette\Utils\Strings;
use Nette\Caching\Cache;
use Doctrine\ORM\EntityManager;
use App\Models\Entities\CategoryBranching;

class ItemCategories
{
    /** @var EntityManager  */
    private $entityManager;
    /** @var Cache  */
    private $cache;

    public function __construct(EntityManager $entityManager){
        $this->entityManager = $entityManager;

        $storage = new Nette\Caching\Storages\FileStorage(__DIR__.'/../../temp/cache');
        $this->cache = new Cache($storage);
    }

    public function getMenu($maxLevel = null){
            //$menuCache = $this->cache->load('categoryMenu', function (&$dependencies) {
            $menu = $this->createArrayMenu($this->entityManager->getRepository(CategoryBranching::class)->getCategoryBranching(), $maxLevel);
         //   $this->cache->save("categoryMenu", $menu);
         //   return $menu;
        //});
        //return $menuCache;
        return $menu;
    }
    public function createArrayMenu(array $elements, $maxLevel = null, $parentId = 0, $parentPath = [], $fullPathIdes = '', $fullPathNames = '', $level = 0) {
        $branch = [];
        foreach ($elements as $element) {
            if ($element['parentId'] == $parentId) {

                $category = [];

                $fullPath = $parentPath;
                $fullPath[] = $element['name'];

                $category['id'] = $element['id'];
                $category['name'] = $element['name'];
                $category['level'] = $level + 1;
                $category['fullPathArray'] = $fullPath;

                if($fullPathNames != ''){
                    $category['fullPath'] = $fullPathNames . '-' . str_replace('-', '_', Strings::webalize($element['name']));
                }
                else{
                    $category['fullPath'] = str_replace('-', '_', Strings::webalize($element['name']));
                }

                if($parentId != '0' && $fullPathIdes != ''){
                    $category['fullPathIdes'] = $fullPathIdes . "_" . $parentId;
                }
                else{
                    $category['fullPathIdes'] = $parentId;
                }

                if($maxLevel === null || $category['level'] < $maxLevel) {
                    $children = $this->createArrayMenu($elements, $maxLevel, $element['id'], $fullPath, $category['fullPathIdes'], $category['fullPath'], $category['level']);

                    if ($children) {
                        $category['children'] = $children;
                    }
                }

                $branch[$category['id']] = (object)$category;
            }
        }

        return $branch;
    }

    public function getAllSubcategories($elements, $parentId){
        $branch = [];
        $branch[] = $parentId;
        foreach ($elements as $element) {
            if ($element['parentId'] == $parentId) {
                $children = $this->getAllSubcategories($elements, $element['id']);

                if ($children) {
                    foreach($children as $child){
                        $branch[] = $child;
                    }
                }
            }
        }
        return $branch;
    }
}