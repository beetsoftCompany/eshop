<?php

namespace App;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;


class RouterFactory
{
	use Nette\StaticClass;

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
		$router = new RouteList;

		$router[] = $backOffice =  new RouteList('BackOffice');
		$backOffice[] = new Route('[<locale=cs cs|en>/]back-office/<presenter>/<action>[/<id>]', [
			'presenter' => [
				Route::VALUE => 'Homepage',
				Route::FILTER_TABLE => [
					'objednavky' => 'Orders',
					'zbozi' => 'Products',
					'statistiky' => 'Statistics',
					'doprava-a-platba' => 'PaymentAndDelivery',
					'nastaveni' => 'Settings'
				],
			],
			'action' => [
				Route::VALUE => 'default',
				Route::FILTER_TABLE => [

				],
			],
			'id' => null,
		]);
		$router[] = $order =  new RouteList('Order');
		$order[] = new Route('[<locale=cs cs|en>/]objednavka/<action>', [
			'presenter' => [
				Route::VALUE => 'Order'
			],
			'action' => [
				Route::VALUE => 'default',
				Route::FILTER_TABLE => [
					'krok1' => 'step1',
					'krok2' => 'step2',
					'krok3' => 'step3',
					'krok4' => 'step4'
				],
			],
			'id' => null,
		]);
		$router[] = new Route('[<locale=cs cs|en>/]<categoryPath>/<categoryId>/produkty', [
			'presenter' => [
				Route::VALUE => 'Homepage'
			],
			'action' => [
				Route::VALUE => 'printProducts'
			]
		]);
		$router[] = new Route('[<locale=cs cs|en>/]<item>/<itemId>/produkt', [
			'presenter' => [
				Route::VALUE => 'Homepage'
			],
			'action' => [
				Route::VALUE => 'product'
			]
		]);
		$router[] = new Route('[<locale=cs cs|en>/]<presenter>/<action>[/<id>]', [
			'presenter' => [
				Route::VALUE => 'Homepage',
				Route::FILTER_TABLE => [

				],
			],
			'action' => [
				Route::VALUE => 'default',
				Route::FILTER_TABLE => [

				],
			],
			'id' => null,
		]);
		return $router;
	}
}
