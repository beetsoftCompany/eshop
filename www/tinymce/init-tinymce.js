tinymce.init({
      selector: '.txtArea-productDescription',
      width: "720px",
  		plugins: "image imagetools media colorpicker textcolor fullscreen table link lists insertdatetime",
  		language: 'cs',
      	insertdatetime_formats: ["%H:%M:%S", "%d-%m-%Y", "%d.%m.%Y", "%d.%m.%Y %H:%M:%S", "%d.%m.%Y %H:%M"],
   		//toolbar: "format forecolor backcolor image media",
  		//menubar: "",
      //toolbar: "insertdatetime",
      file_browser_callback : function(field_name, url, type, win){
          var filebrowser = "filebrowser.php?select=art";
          filebrowser += (filebrowser.indexOf("?") < 0) ? "?type=" + type : "&type=" + type;
          tinymce.activeEditor.windowManager.open({
            title : "Výběr obrázku",
            width : 520,
            height : 400,
            url : filebrowser
          }, {
              window : win,
              input : field_name
          });
          return false;
      }
	});
//https://pixabay.com/cs/blog/posts/direct-image-uploads-in-tinymce-4-42/
/*
file_browser_callback: function(field_name, url, type, win) {
          if(type=='image') $('#my_form input').click();
      },
*/

//contenteditable="true"