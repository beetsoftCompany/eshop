/**
 * Created by Radek on 26.12.2017.
 */
var isEdit = false;
$(function(){
    $(".div-product").on("click", '.editableField', function(){
        if(isEdit == false){
            isEdit = true;
            var height = $(this).height();
            $(this).find(".editableValue").css("display", "none");
            $(this).find(".editableFieldInput").css("display", "block");
            $(this).find(".editableFieldInput").height(height);
            $(this).find(".editableFieldInput").outerHeight($(this).outerHeight());
            $(this).find(".editableFieldInput").css("font-size", height);
        }
    });
    $(".div-product").on("mouseleave", '.editableField', function(){
        isEdit = false;
        $(this).find(".editableValue").css("display", "block");
        $(this).find(".editableFieldInput").css("display", "none");
    });
});