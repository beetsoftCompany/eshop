/**
 * Created by Radek on 07.11.2017.
 */
$(function(){
    //$(".link-addCategory").on("mouseover", function(){
    /*
    $("#div-categoryMenu").on("click", '.link-addCategory', function(){
        var categoryFullPath = $(this).attr("data-categoryFullPath");
        $(this).css("display", "none");
        $("#frm-categoriesMenu-addMenuCategoryForm-" + categoryFullPath).css("display", "block");
    });
    $("#div-categoryMenu").on("mouseleave", '.addMenuCategoryForm', function(){
        $(".link-addCategory").css("display", "inline");
        $(".addMenuCategoryForm").css("display", "none");
    });*/
    initCategoryMenu();
    $.nette.ext('', {
        complete: function (payload) {
            if ($.inArray("snippet-categoriesMenu-categoryMenu", payload.snippets)) {
                initCategoryMenu();
            }
        }
    });
});
function initCategoryMenu(){
    $("#div-categoriesMenu .treeview-menu").sortable({
        //revert: true
        connectWith: ".treeview-menu",
        cancel: ".li-categoriesMenu-addCategory",
        beforeStop: function(event, ui){
            console.log(ui.item.attr("data-categoryPath"));
        }
    });
}