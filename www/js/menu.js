/**
 * Created by uzivatel on 18.01.2018.
 */
$(document).ready(function(){
    $("#category-menu-button").click(function(){
       var now = $("#div-categoryMenu").css("display");
        if (now == "none")
        {
            $("#div-categoryMenu").css("display", "block");
        }
        else
        {
            $("#div-categoryMenu").css("display", "none");
        }
    });
    $("#kosik-a").click(function(){
        var now = $("#kosik").css("display");
        if (now == "none")
        {
            $("#kosik").css("display", "block");
        }
        else
        {
            $("#kosik").css("display", "none");
        }
    });
});
