/**
 * Created by Radek on 02.12.2017.
 */
$(function () {
    $("#div-showUploadedImage").on('click', '.itemUploadedImg', function(){
        $("#div-showUploadedImage .titleImg").removeClass('titleImg');
        $(this).addClass('titleImg');
        $("#frm-addItem input[name=titleImg]").val($(this).attr('data-imgName'));
        console.log('Change title image');
    });

    $("#frm-addItem-images").change(function(evt){
        if (getImagesSize(evt.target.files) > post_max_filesize){
            alert('Soubory musí být menší než ' + bytesToSize(post_max_filesize));
            $(this).val([]);
            $("#div-showUploadedImage").html('');
        }
        else{
            displayUploadedImages(evt);
        }
    });
});

function displayUploadedImages(evt){
    $("#div-showUploadedImage").html('');
    var uploadedImages = evt.target.files;
    for (var i = 0, f; f = uploadedImages[i]; i++) {
        var fr = new FileReader();
        var uploaded = 0;/*
         fr.onload = function(ev) {
         $("#div-showUploadedImage").append("<img src='" + ev.target.result + "' class='itemUploadedImg' >" + ev.name);
         };*/
        fr.onload = (function(file) {
            return function(e) {
                $("#div-showUploadedImage").append("<img src='" + e.target.result + "' class='itemUploadedImg' data-imgName='" + file.name + "'>");
            };
        })(f);
        fr.onprogress = function(ev){

        };
        fr.onloadend = (function(file){
            return function (ev){
                if(uploaded == 0){
                    $("#div-showUploadedImage .itemUploadedImg:first-child").addClass('titleImg');
                    $("#frm-addItem input[name='titleImg']").val(file.name);
                }
                uploaded++;
            }
        })(f);
        fr.readAsDataURL(f);
    }
}
function getImagesSize(images){
    var uploadedImagesSize = 0;
    for (var i = 0, f; f = images[i]; i++) {
        uploadedImagesSize += f.size;
    }
    return uploadedImagesSize;
}

function bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0) return '0 Byte';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}