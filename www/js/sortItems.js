$(function(){
    var loaderObj = new loader();

    var minPrice = parseInt($("#slider-priceRange").attr("data-minPrice"));
    var maxPrice = parseInt($("#slider-priceRange").attr("data-maxPrice"));

    $("#slider-priceRange").slider({
        range: true,
        //step: 100,
        min: minPrice,
        max: maxPrice,
        values: [minPrice, maxPrice],
        classes: {
            "ui-slider-handle": "slidePriceRangeHandle"
        },
        create: function(event, ui) {
            $("#slider-priceRange .ui-slider-handle:first").html("Od");
            $("#slider-priceRange .ui-slider-handle:last").html("Do");
        },
        slide: function(event, ui) {
            $("#span-minPrice").html(ui.values[0] + ",-");
            $("#span-maxPrice").html(ui.values[1] + ",-");
        },
        change: function(event, ui){
            $.nette.ajax({
                url: "?do=changeSort",
                data : {
                    sortType: "priceRange",
                    minPrice: ui.values[0],
                    maxPrice: ui.values[1],
                    currentSort: $("#div-sortMenu").attr("data-currentSort")
                },
                start: function(){
                    loaderObj.start();
                },
                success: function (payload) {
                    loaderObj.stop();
                    //history.pushState(null, '', '/en/step2');
                }
            });
        }
    });
    $.nette.ext('sortItemsEvent', {
        start: function(){
            loaderObj.start();
        },
        success: function (data) {
            loaderObj.stop();
        }
    });/*
    $(".a-sortType").on("click", function(){
        $.nette.ext('sortItemsEvent');
    });*/

});
//jQuery.param.querystring(window.location.href, 'valueA=321&valueB=123');