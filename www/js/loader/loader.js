/**
 * Created by Radek on 06.11.2017.
 */
function loader(){
    this.start = function(){
        var css_link = $("<link>", {
            rel: "stylesheet",
            type: "text/css",
            href: getScriptPath("loader.js") + "loader.css",
            id: "loaderCss"
        });
        css_link.appendTo('head');
        $("body").append("<div class='loader'></div>");
    };
    this.stop = function(){
        $(".loader, #loaderCss").remove();
    };
}

function getScriptPath(jsfile) {

    var scriptElements = document.getElementsByTagName('script');
    var i, element, myfile;

    for(i = 0; element = scriptElements[i]; i++) {

        myfile = element.src;

        if(myfile.indexOf(jsfile) >= 0 ) {
            var myurl = myfile.substring(0, myfile.indexOf(jsfile));

        }
    }
    return myurl;
}