<?php

namespace Test;

use Nette;
use \Tester\TestCase;
use Tester\Assert;
use Mockery;

use App\Models\Settings;

require __DIR__ . '/bootstrap.php';

class SettingsTest extends TestCase
{
    protected function tearDown()
    {
        parent::tearDown();
        Mockery::close();
    }

    public function testGetSetting()
    {
        $selection = Mockery::mock(Nette\Database\Table\Selection::class);
        $selection->shouldReceive('insert')->once();

        $context = Mockery::mock(Nette\Database\Context::class);
        $context->shouldReceive('table')->once()->andReturn($selection);

        $context2 = Mockery::mock(Nette\Http\Session::class);
        $context2->shouldReceive('session')->never();

        $settings = new Settings($context, $context2);


        //$context = Mockery::mock(Conte)
        //$settings = $this->container->getByType(Settings::class);
        Assert::equal("21", $settings->getSetting('dph-1'));
    }
}
(new SettingsTest())->run();